import pandas as pd
import os
from tensorflow.keras.models import load_model

from foundation.configuration.input import Input

class FileExtractor:

    def __init__(self):
        # self.users_steps_csv_filename = Input.get_instance().inputs['users_steps_filename']
        # self.ground_truth_filename = Input.get_instance().inputs['ground_truth']
        pass

    def read_csv(self, filename):

        df = pd.read_csv(filename)

        return df

    def read_multiples_csv(self, dir):

        files = [os.listdir(dir)[0]]

        concat_df = None
        for file in files:
            df = self.read_csv(dir+file)
            if concat_df is None:
                concat_df = df
            else:
                concat_df = pd.concat([concat_df, df], ignore_index=True)

        return concat_df


    def extract_ground_truth_from_csv(self):
        df = pd.read_csv(self.ground_truth_filename)

        return df

    def read_model(self, filename):

        return load_model(filename)