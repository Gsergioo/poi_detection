import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import copy
import datetime as dt
from .configurations import foursquare_dataset_nye, MIN_DATE, MAX_DATE

def count_user_weekday(e):
    dict_count = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return dict_count

def count_user_week_of_year(e):
    dict_count = {i:0 for i in range(53)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return dict_count

def count_user_hour(e):
    dict_count = {e:0 for e in range(24)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return dict_count

def count_user_year(e):
    dict_count = {e:0 for e in range(2012,2014)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return dict_count

def count_user_month(e):
    dict_count = {e:0 for e in range(1, 13)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return dict_count

def user_time_between_events(data):
    data = data.sort_values()
    diffs = []
    for i in range(1, len(data)):
        diff = (data.iloc[i] - data.iloc[i-1]).total_seconds()/3600
        diffs.append(diff)

    return diffs

if __name__ == "__main__":

    weekday = True
    hour = True
    year = True
    month = True

    min_year=2012
    max_year=2013
    min_month = 12
    max_month = 2

    df = pd.read_csv(foursquare_dataset_nye)
    columns = ['userId', 'venueId', 'utcTimestamp', 'latitutde', 'longitude', 'venueCategory']
    df = df[['userId', 'venueId', 'utcTimestamp', 'venueCategory']]
    df['utcTimestamp'] = pd.to_datetime(df['utcTimestamp'], infer_datetime_format=True)

    # time between events
    df_time_between_events = copy.copy(df)
    df_time_between_events = df_time_between_events[df_time_between_events['utcTimestamp'] >= MIN_DATE]
    df_time_between_events = df_time_between_events[df_time_between_events['utcTimestamp'] < MAX_DATE]
    #df_week_of_year['utcTimestamp'] = df_week_of_year['utcTimestamp'].apply(lambda e: str(e.weekofyear))
    # ids = df_week_of_year['userId'].unique().tolist()
    df_time_between_events = df_time_between_events.groupby(by='userId').apply(lambda e: user_time_between_events(e['utcTimestamp']))
    time_between_events_lists = df_time_between_events.tolist()
    concat = []
    for e in time_between_events_lists:
        concat+=e
    time_between_events_lists = concat
    ax = plt.subplots()[1]
    figure = sns.boxplot(data=time_between_events_lists, ax=ax)
    figure.set_title("Tempo entre eventos consecutivos (" + str(MIN_DATE.year) + ")")
    figure = figure.get_figure()
    figure.savefig("boxplot_time_between_events.png", bbox_inches='tight', dpi=400)

    # weekday
    if weekday:
        df_weekday = copy.copy(df)
        df_weekday = df_weekday[df_weekday['utcTimestamp'] >= MIN_DATE]
        df_weekday = df_weekday[df_weekday['utcTimestamp'] < MAX_DATE]
        df_weekday['utcTimestamp'] = df_weekday['utcTimestamp'].apply(lambda e: str(e.weekday()))
        ids = df_weekday['userId'].unique().tolist()
        df_weekday = df_weekday.groupby(by='userId').apply(lambda e: count_user_weekday(e['utcTimestamp']))
        weekdays_dicts = df_weekday.tolist()

        dict_count = {0: "Segunda", 1: "Terça", 2: "Quarta", 3: "Quinta", 4: "Sexta", 5: "Sábado", 6: "Domingo"}
        value = []
        day = []
        for dict_ in weekdays_dicts:
            for e in dict_:
                value.append(dict_[e])
                day.append(dict_count[e])


        count_weekday = pd.DataFrame({'weekday': day,'value': value})
        sns.set()
        ax = plt.subplots()[1]
        figure = sns.barplot(data=count_weekday, x='weekday', y='value', ax=ax, ci=95)
        figure.set_title("Eventos dias da semana (agosto 2010)")
        figure = figure.get_figure()
        figure.savefig("distplot_date.png", bbox_inches='tight', dpi=400)

    # hour
    if hour:
        df_hour = copy.copy(df)
        df_hour = df_hour[df_hour['utcTimestamp'] >= MIN_DATE]
        df_hour = df_hour[df_hour['utcTimestamp'] < MAX_DATE]
        df_hour['utcTimestamp'] = df['utcTimestamp'].apply(lambda e: str(e.hour))
        df_hour = df_hour.groupby(by='userId').apply(lambda e: count_user_hour(e['utcTimestamp']))

        hours_dicts = df_hour.tolist()
        value = []
        hour = []
        for dict_ in hours_dicts:
            for e in dict_:
                value.append(dict_[e])
                hour.append(e)

        count_year = pd.DataFrame({'hour': hour, 'value': value})
        ax = plt.subplots()[1]
        figure = sns.barplot(data=count_year, x='hour', y='value', ax=ax, ci=95)
        figure.set_title("Eventos por hora (agosto 2010)")
        figure = figure.get_figure()
        figure.savefig("distplot_hora.png", bbox_inches='tight', dpi=400)

    # year
    if year:
        df_year = copy.copy(df)
        df_year['utcTimestamp'] = df['utcTimestamp'].apply(lambda e: str(e.year))
        df_year = df_year.groupby(by='userId').apply(lambda e: count_user_year(e['utcTimestamp']))

        year_dicts = df_year.tolist()
        value = []
        year = []
        for dict_ in year_dicts:
            for e in dict_:
                value.append(dict_[e])
                year.append(e)

        count_year = pd.DataFrame({'year': year, 'value': value})
        ax = plt.subplots()[1]
        figure = sns.barplot(data=count_year, x='year', y='value', ax=ax, ci=95)
        figure.set_title("Eventos por ano")
        figure = figure.get_figure()
        figure.savefig("distplot_year.png", bbox_inches='tight', dpi=400)

    # month
    if month:
        df_month = copy.copy(df)
        df_month = df_month[df_month['utcTimestamp'] >= MIN_DATE]
        df_month = df_month[df_month['utcTimestamp'] < MAX_DATE]
        df_month['utcTimestamp'] = df['utcTimestamp'].apply(lambda e: str(e.month))
        df_month = df_month.groupby(by='userId').apply(lambda e: count_user_month(e['utcTimestamp']))

        month_dicts = df_month.tolist()
        value = []
        month = []
        for dict_ in month_dicts:
            for e in dict_:
                value.append(dict_[e])
                month.append(e)

        count_month = pd.DataFrame({'month': month, 'value': value})
        ax = plt.subplots()[1]
        figure = sns.barplot(data=count_month, x='month', y='value', ax=ax, ci=95)
        figure.set_title("Eventos por mês (" + str(MIN_DATE.year) + ")")
        figure = figure.get_figure()
        figure.savefig("distplot_month.png", bbox_inches='tight', dpi=400)