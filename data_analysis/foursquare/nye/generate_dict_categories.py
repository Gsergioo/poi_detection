import pandas as pd

if __name__ == "__main__":

    dir = "foursquare_osm_categories.csv"
    df = pd.read_csv(dir)
    # 'venueUniqueCategory', 'OSM_1st category', 'OSM 2sd category',
    #        'Reduced_OSM_1st category', 'Foursquare_category'
    column = 'Reduced_OSM_1st category'
    print("OSM_1st category: ", len(df['OSM_1st category'].unique().tolist()),
          " Reduced_OSM_1st category: ", len(df['Reduced_OSM_1st category'].unique().tolist()))
    df = df.dropna(subset=[column])
    df.to_csv("foursquare_osm_categories.csv", index_label=False, index=False)
    category_to_osm = {}
    unique_categories = df[column].unique().tolist()
    osm_to_int = {unique_categories[i]: i for i in range(len(unique_categories))}
    print("Mapeamento categorias unicas: ", osm_to_int)
    category_to_int_osm = {}
    for i in range(df.shape[0]):
        row = df.iloc[i]
        category = row['venueUniqueCategory']
        osm = row[column]
        category_to_osm[category] = osm
        category_to_int_osm[category] = osm_to_int[category_to_osm[category]]

    print(category_to_osm)
    print("")
    print(category_to_int_osm)