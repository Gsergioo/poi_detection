import pandas as pd
import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('/home/claudio/Documentos/pycharmprojects/projeto_guilherme/poi_detection/')
import seaborn as sns
from configurations import foursquare_dataset_nye
from configuration.base_poi_categorization_configuration import BasePoiCategorizationConfiguration


def barplot(metrics, x_column, y_column, file_name):
    figure = sns.barplot(x=x_column, y=y_column, data=metrics, ci=95)
    figure = figure.get_figure()
    figure.savefig(file_name + ".png", bbox_inches='tight', dpi=400)

def category_name_to_int(category, category_to_int):

    integer_categories = []
    for c in category:
        integer_categories.append(categories_to_int[c])

    return integer_categories

def summaryze(df, topk):
    df = df.sort_values('total', ascending=False)
    if df.shape[0] < topk:
        return pd.DataFrame({'sum_topk': [0], 'sum_events': [0],
                             'topk': [topk], 'total_pois': [df.shape[0]],
                             'sum_topk_percentage': [0], 'topk_percentage': [0],
                             'last_topk': [0]})
    sum_topk = sum(df['total'].tolist()[:topk])
    last_topk = df['total'].tolist()[topk-1]
    sum_events = sum(df['total'].tolist())

    return pd.DataFrame({'sum_topk': [sum_topk], 'sum_events': sum_events,
                         'topk': [topk], 'total_pois': [df.shape[0]],
                         'sum_topk_percentage': [sum_topk/sum_events],
                         'topk_percentage': [topk/df.shape[0]],
                         'last_topk': [last_topk]})


if __name__ == "__main__":
    topks = [6, 7, 8, 9, 10, 11, 12, 13, 14]
    min_date = pd.Timestamp(year=2012, month=4, day=1, tz='utc')
    max_date = pd.Timestamp(year=2012, month=6, day=1, tz='utc')

    base_config = BasePoiCategorizationConfiguration()
    df = pd.read_csv(foursquare_dataset_nye)
    categories_to_int = base_config.FOURSQUARE_CATEGORIES_NAMES_TO_INT_OSM_FIRST_LEVEL_13_CATEGORIES
    categories_name = list(categories_to_int.keys())
    print(df.columns)
    # 'userId', 'venueId', 'venueCategoryId', 'venueCategory', 'latitude',
    #        'longitude', 'timezoneOffset', 'utcTimestamp'
    """ Pré-processing"""
    df = df[['userId', 'venueId', 'venueCategoryId', 'venueCategory', 'utcTimestamp']]
    df = df.query("venueCategory in " + str(categories_name))
    df['utcTimestamp'] = pd.to_datetime(df['utcTimestamp'], infer_datetime_format=True)
    df = df[df['utcTimestamp'] >= min_date]
    df = df[df['utcTimestamp'] <= max_date]
    print("tamanho 2: ", df.shape)
    df = df.drop_duplicates()
    print("tamanho 3: ", df.shape)
    print("Quantidade inicial de usuários: ", len(df['userId'].unique().tolist()))
    df = df.groupby(by=['userId', 'venueId']).apply(lambda e: len(e))
    #print(df)
    df = df.reset_index()
    df.columns = ['userId', 'venueId', 'total']
    #print(df)
    select_columns = ['sum_topk',  'sum_events',  'topk',  'total_pois',
                      'sum_topk_percentage',  'topk_percentage',  'last_topk']
    topk_dfs = None
    for topk in topks:
        df_1 = df.groupby(by='userId').apply(lambda e: summaryze(e[['venueId', 'total']], topk)).query("sum_topk > 0")
        df_1 = df_1[select_columns]
        print("quantidade final de usuários: ", df_1.shape[0])
        if topk_dfs is not None:
            topk_dfs = pd.concat([topk_dfs, df_1], ignore_index=True)
        else:
            topk_dfs = df_1
    print("Describe quantidade de pois por usuário: ",
          df.groupby(by='userId').apply(lambda e: pd.DataFrame({'pois_per_user': [len(e)]}))['pois_per_user'].describe())
    print("Describe quantidade de eventos por usuário: ",
          df.groupby(by='userId').apply(lambda e: pd.DataFrame({'events_per_user': [e['total'].sum()]}))[
              'events_per_user'].describe())
    print("summaryze\n", topk_dfs)
    filename = "sum_topk_percentage"
    barplot(topk_dfs, 'topk', 'sum_topk_percentage', filename)
    filename = "last_topk"
    barplot(topk_dfs, 'topk', 'last_topk', filename)