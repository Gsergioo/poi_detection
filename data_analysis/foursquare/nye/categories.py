import pandas as pd

if __name__ == "__main__":

    filename = "/home/claudio/Documentos/datasets_redes_neurais/fourquare_2012_2013/dataset_TSMC2014_NYC.csv"
    df = pd.read_csv(filename)
    print(df)
    columns = df.columns
    #['userId', 'venueId', 'venueCategoryId', 'venueCategory', 'latitude',
    # 'longitude', 'timezoneOffset', 'utcTimestamp']
    print(columns)
    print("Categorias: ", df['venueCategory'].unique().tolist())
    pd.DataFrame({"venueUniqueCategory": df['venueCategory'].unique().tolist()}).to_csv("foursquare_categories.csv", index_label=False, index=False)

