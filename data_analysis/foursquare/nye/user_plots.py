import pandas as pd
from configurations import foursquare_dataset_nye
import matplotlib.pyplot as plt
import seaborn as sns

def duration(data):

    duration = (data.max() - data.min()).days

    return duration

def freq_events_per_day(data):

    events = data.shape[0]
    duration = (data.max() - data.min()).days
    if duration == 0:
        return 0
    return events/duration

if __name__ == "__main__":

    df = pd.read_csv(foursquare_dataset_nye)
    columns = ['userId', 'venueId', 'utcTimestamp', 'latitutde', 'longitude', 'venueCategory']
    df['utcTimestamp'] = pd.to_datetime(df['utcTimestamp'], infer_datetime_format=True)
    print("------ Metricas ------")
    df = df[['userId', 'venueId', 'utcTimestamp', 'venueCategory']]
    print("quantidade de usuários: ", len(df['userId'].unique().tolist()))
    print("quantidade de pois: ", len(df['venueId'].unique().tolist()))
    print("quantidade de categorias: ", len(df['venueCategory'].unique().tolist()))
    print("describe datetime: ", df['utcTimestamp'].describe())

    groupby_userid_duration = df.groupby(by='userId').apply(lambda e: duration(e['utcTimestamp']))
    groupby_userid_duration = groupby_userid_duration[groupby_userid_duration>0]
    groupby_userid_freq_events_per_day = df.groupby(by='userId').apply(lambda e: freq_events_per_day(e['utcTimestamp']))
    groupby_userid_freq_events_per_day = groupby_userid_freq_events_per_day[groupby_userid_freq_events_per_day>0]

    print("Duracao: ", groupby_userid_duration.describe())
    ax = plt.subplots()[1]
    print("times: ", groupby_userid_duration)
    figure = sns.boxplot(data=groupby_userid_duration.tolist())
    figure.set_title("Duração (dias)")
    figure = figure.get_figure()
    figure.savefig("boxplot_days.png", bbox_inches='tight', dpi=400)

    print("Frequencia de eventos por dia: ", groupby_userid_freq_events_per_day.describe())
    ax = plt.subplots()[1]
    figure = sns.boxplot(data=groupby_userid_freq_events_per_day.tolist())
    figure.set_title("Freq. de eventos por dia")
    figure = figure.get_figure()
    figure.savefig("boxplot_freq_events_per_day.png", bbox_inches='tight', dpi=400)
