import pandas as pd
from configurations import weeplaces_dataset
import matplotlib.pyplot as plt
import seaborn as sns

def duration(data):

    duration = (data.max() - data.min()).days

    return duration

def freq_events_per_day(data):

    events = data.shape[0]
    duration = (data.max() - data.min()).days
    if duration == 0:
        return 0
    return events/duration

if __name__ == "__main__":

    df = pd.read_csv(weeplaces_dataset)
    columns = ['userid', 'placeid', 'datetime', 'lat', 'lon', 'city', 'category']
    df['datetime'] = pd.to_datetime(df['datetime'], infer_datetime_format=True)
    print("------ Metricas ------")
    df = df[['userid', 'placeid', 'datetime', 'city', 'category']]
    print("quantidade de usuários: ", len(df['placeid'].unique().tolist()))
    print("quantidade de cidades: ", len(df['city'].unique().tolist()))
    print("quantidade de categorias: ", len(df['category'].unique().tolist()))
    print("describe datetime: ", df['datetime'].describe())

    groupby_userid_duration = df.groupby(by='userid').apply(lambda e: duration(e['datetime']))
    groupby_userid_duration = groupby_userid_duration[groupby_userid_duration>0]
    groupby_userid_freq_events_per_day = df.groupby(by='userid').apply(lambda e: freq_events_per_day(e['datetime']))
    groupby_userid_freq_events_per_day = groupby_userid_freq_events_per_day[groupby_userid_freq_events_per_day>0]

    print("Duracao: ", groupby_userid_duration.describe())
    ax = plt.subplots()[1]
    figure = sns.boxplot(data=groupby_userid_duration)
    figure.set_title("Duração (dias)")
    figure = figure.get_figure()
    figure.savefig("boxplot_days.png", bbox_inches='tight', dpi=400)

    print("Frequencia: ", groupby_userid_freq_events_per_day.describe())
    ax = plt.subplots()[1]
    figure = sns.boxplot(data=groupby_userid_freq_events_per_day)
    figure.set_title("Freq. de eventos por dia")
    figure = figure.get_figure()
    figure.savefig("boxplot_freq_events_per_day.png", bbox_inches='tight', dpi=400)
