import pandas as pd
import datetime


BASE_DIR = "/media/claudio/Acer/Users/claud/Downloads/doutorado/global_foursquare/dataset_TIST2015/"

CHECK_INS_DATASET = BASE_DIR+"dataset_TIST2015_Checkins.txt"

POIS_DATASET = BASE_DIR+"dataset_TIST2015_Checkins.txt"

MIN_DATE = pd.Timestamp(year=2010, month=6, day=29, tz='utc')
MAX_DATE = pd.Timestamp(year=2010, month=9, day=2, tz='utc')

MONTH_7_DATE = pd.Timestamp(year=2010, month=7, day=1, tz='utc')
MONTH_8_DATE = pd.Timestamp(year=2010, month=9, day=1, tz='utc')

MIN_DATETIME = datetime.datetime(2010, 6, 29, 0, 0, 0)
MAX_DATETIME = datetime.datetime(2010, 9, 2, 0, 0, 0)