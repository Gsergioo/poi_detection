import statistics as st
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans, SpectralClustering, AgglomerativeClustering
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import preprocessing
from sklearn import metrics
import math
from sklearn.metrics.pairwise import haversine_distances
from scipy import stats
import folium
from folium import plugins
import io
from PIL import Image
from configurations import weeplaces_dataset, MONTH_7_DATE, MONTH_8_DATE, \
    weeplaces_dataset_local_datetime_folder, WEEPLACES_DATASET_PREPROCESSED

if __name__ == "__main__":

    user_id_column = 'userid'
    datetime_column = 'local_datetime'
    latitude_column = 'lat'
    longitude_column = 'lon'
    category_column = 'category'

    df = pd.read_csv(WEEPLACES_DATASET_PREPROCESSED)
    columns = ['userid', 'placeid', 'datetime', 'lat', 'lon', 'city', 'category', 'local_datetime']
    df.columns = ['userid', 'placeid', 'datetime', 'lat', 'lon', 'city', 'category', 'local_datetime']
    df['local_datetime'] = pd.to_datetime(df['local_datetime'], utc=True, infer_datetime_format=True)
    df = df[df[datetime_column] >= MONTH_7_DATE]
    df = df[df[datetime_column] < MONTH_8_DATE]

    latitude = df[latitude_column].tolist()
    longitude = df[longitude_column].tolist()
    coordenadas = df[[latitude_column, longitude_column]].values

    m = folium.Map([st.mean(latitude), st.mean(longitude)], zoom_start=1)
    m.add_child(plugins.HeatMap(coordenadas, min_opacity=2, max_zoom=1,
                 max_val=2.0, radius=1, blur=1))
    # img_data = m._to_png(5)
    # img = Image.open(io.BytesIO(img_data))
    # img.save('heatmap.png')

    m.save("heatmap_eventos.html")

