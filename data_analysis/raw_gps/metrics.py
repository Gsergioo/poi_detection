import statistics as st
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans, SpectralClustering, AgglomerativeClustering
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import preprocessing
from sklearn import metrics
import math
from sklearn.metrics.pairwise import haversine_distances
from scipy import stats
from configurations import RAW_GPS_FILENAME

def duration(data):

    duration = (data.max() - data.min()).days

    return duration

def detect_outliers(data, m=1.5):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d / (mdev if mdev else 1.)
    return data[s < m]

def average_time_between_events(data):
    data = data.sort_values()
    diffs = []
    for i in range(1, len(data)):
        diff = (data.iloc[i] - data.iloc[i-1]).total_seconds()/3600
        diffs.append(diff)

    return diffs

def cluster_users_by_time_between_events(data):
    pass

def normalize_df(df):
    min_max_scaler = preprocessing.MinMaxScaler()

    columns = df.columns
    new_df = {e:[] for e in columns}
    for c in columns:
        if c == 'user_id':
            new_df[c] = df[c]
            continue
        column = df[c].astype('float64').tolist()
        maximum = max(column)
        minimum = min(column)
        column_normalized = [(e-minimum)/(maximum-minimum) for e in column]
        new_df[c] = column_normalized

    new_df = pd.DataFrame(new_df)
    return new_df

def calc_lat_long_avg_and_radius(df, latitude_colunm, longitude_column):
    # longitude average
    size = df.shape[0]
    latitude = df[latitude_colunm]
    longitude = df[longitude_column]
    zeta_sum = sum([math.sin(math.radians(longitude)) for longitude in longitude]) / len(longitude)
    xi_sum = sum([math.cos(math.radians(long)) for long in longitude]) / len(longitude)
    long_average = math.degrees(math.atan2(zeta_sum, xi_sum))

    # latitude average
    lat_average = latitude.mean()

    # radius based on haversine mean
    radius = 0
    for location in zip(df.latitude, df.longitude):
        hav = haversine_distances([(lat_average, long_average), location])
        radius += hav[0, 1]*6371000
    radius = radius / size

    # host app package

    return radius

if __name__ == "__main__":

    user_id_column = 'userid'
    datetime_column = 'datetime'
    latitude_column = 'latitude'
    longitude_column = 'longitude'
    category_column = 'category'

    df = pd.read_csv(RAW_GPS_FILENAME)
    print("colunas: ", df.columns)
    df[datetime_column] = pd.to_datetime(df[datetime_column], utc=True, infer_datetime_format=True)
    print("------ Metricas ------")
    print("quantidade de usuários: ", len(df['poi_id'].unique().tolist()))
    print("describe datetime: ", df[datetime_column].describe())
    print(df.columns)
    print(df)

    groupby_userid = df.groupby(by=user_id_column).apply(lambda e: duration(e[datetime_column]))
    print("duracao: ", groupby_userid.describe())

    radius = df.groupby(by=user_id_column). \
        apply(lambda e: calc_lat_long_avg_and_radius(e[[latitude_column, longitude_column]], latitude_column,
                                                     longitude_column)).tolist()
    groupby_userid = df.groupby(by=user_id_column).apply(
        lambda e: average_time_between_events(e[datetime_column])).reset_index()
    print("colunas: ", groupby_userid.columns)
    print("duracao média entre eventos: ", groupby_userid)

    time_between_events_lists = groupby_userid[0].tolist()
    concat = []
    for e in time_between_events_lists:
        concat += e

    features_original = {}
    features_original["user_id"] = []
    features_original["average"] = []
    features_original["cv"] = []
    features_original["median"] = []
    # features_original["radius"] = radius
    k = 2
    for i in range(len(time_between_events_lists)):
        e = time_between_events_lists[i]
        if len(e) < 2:
            continue
        features_original['user_id'].append(groupby_userid.iloc[i][user_id_column])
        average = st.mean(e)
        dev = st.stdev(e)
        cv = dev / average
        median = st.median(e)
        features_original['average'].append(average)
        features_original['cv'].append(cv)
        features_original['median'].append(median)

    features_original['z_score_cv'] = stats.zscore(features_original['cv'])

    user_metrics_df = normalize_df(pd.DataFrame(features_original))
    #user_metrics_df['user_id'] = pd.Series(groupby_userid['userId'].tolist())
    features_df = pd.DataFrame(user_metrics_df)[['average', 'cv']]
    print("Colunas: ", features_df.columns, " k: ", k)
    # features_df = normalize_df(features_df)
    features = features_df.to_numpy()
    # spektral cv e median
    kmeans = KMeans(n_clusters=k).fit(features)
    labels = kmeans.labels_
    user_metrics_df['label'] = pd.Series(labels)
    features_df['label'] = pd.Series(labels)
    print("Sihoulete coef: ", metrics.silhouette_score(features, labels, metric='euclidean'))
    print("Sulhouete samples: ", metrics.silhouette_samples(features, labels, metric='euclidean'))
    print(labels)
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.set_xlabel("Average", fontsize=18)
    # ax.set_ylabel("Cv", fontsize=18)
    fig = sns.scatterplot(x='average', y='cv', hue='label', data=features_df, s=10, cmap='viridis')
    fig = fig.get_figure()
    fig.savefig("clustering.png")
    plt.show()

    user_metrics_df = user_metrics_df.sort_values(by='user_id')
    user_metrics_df.to_csv(
        "/home/claudio/Documentos/pycharmprojects/projeto_guilherme/poi_detection/files/raw_gps_user_metrics.csv",
        index_label=False,
        index=False)
