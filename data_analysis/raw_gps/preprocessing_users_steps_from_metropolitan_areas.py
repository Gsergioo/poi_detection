import os
import pandas as pd
import geopandas as gpd

from configurations import SHP_BRAZIL_STATES

if __name__ == "__main__":

    STATES = ['SÃO PAULO', 'RIO DE JANEIRO', 'MINAS GERAIS']
    cities = gpd.read_file(SHP_BRAZIL_STATES)
    cities = cities.query("NM_ESTADO in " + str(STATES))
    print("colunas cities: ", cities)

    dir = "/media/claudio/Acer/Users/claud/Downloads/user_steps_01112019_31122019/"
    # files = os.listdir(dir)
    #
    # columns = ['advertising_id', 'event_date',
    #    'local_time', 'latitude', 'longitude',
    #    'location_provider', 'type', 'same_location_count', 'date_br']
    # df = pd.read_csv(dir+files[0])[columns]
    # print("tamanho: ", len(files))
    # for i in range(1, len(files)):
    #     df_file = gpd.read_file(dir+files[i])[columns]
    #     df_file = df_file.query("local_time == 'America/Sao_Paulo'")
    #     print(i)
    #     df = pd.concat([df, df_file], ignore_index=True)
    #
    # print(df)
    #
    # df.to_csv(dir+"users_steps_concat_11_2019.csv")

    df = pd.read_csv(dir+"users_steps_concat_11_2019.csv")
    colunas = df.columns.tolist()
    colunas.append('NM_ESTADO')
    df = gpd.GeoDataFrame(
    df, geometry=gpd.points_from_xy(df.longitude, df.latitude), crs="EPSG:4326")

    df = gpd.sjoin(cities, df, op='contains')
    df = df[colunas]
    df.to_csv("/media/claudio/Acer/Users/claud/Downloads/doutorado/raw_gps/users_steps_of_metropolitan_areas/users_steps_concat_11_2019_sudeste.csv", index=False)
    print("final")




