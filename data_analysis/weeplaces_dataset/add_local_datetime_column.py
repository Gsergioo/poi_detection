import time
import datetime as dt
import pytz
import pandas as pd
import numpy as np
from configurations import weeplaces_dataset, weeplaces_dataset_local_datetime_folder, \
    MIN_DATE, MAX_DATE, MIN_DATETIME, MAX_DATETIME
from timezonefinder import TimezoneFinder

# from pyspark.sql.functions import udf
# from pyspark.sql.types import StringType
# from pyspark.sql import SparkSession
# from pyspark import sql, SparkConf, SparkContext

def convert_tz(datetime, from_tz, to_tz):

    datetime = datetime.replace(tzinfo=from_tz)
    datetime = datetime.astimezone(pytz.timezone(to_tz)).to_pydatetime()
    year = datetime.year
    month = datetime.month
    day = datetime.day
    hour = datetime.hour
    minute = datetime.minute
    second = datetime.second
    #datetime = str(year)+"-"+str(month)+"-"+str(day)+" "+str(hour)+":"+str(minute)+":"+str(second)
    datetime = str(dt.datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=second))
    #datetime = datetime.srtftime("%Y-%m-%d %H:%M:%S")
    #.srtftime("%Y-%m-%d %H:%M:%S")
    return datetime

def find_timezone(datetime, lat, lon):

    tf = TimezoneFinder()

    timezone = tf.timezone_at(lng=lon, lat=lat)
    if timezone is None:
        return 'invalid'
    if len(timezone) == 0:
        return 'invalid'
    local_datetime = str(convert_tz(datetime, pytz.UTC, timezone))
    return local_datetime

if __name__ == "__main__":

    inicio = time.time()
    # spark = SparkSession \
    #     .builder \
    #     .appName("t") \
    #     .getOrCreate()
    #
    #
    # spark_df = spark.read.csv(weeplaces_dataset, inferSchema=True, header=True)
    # spark_df = spark_df.filter(spark_df.datetime >= MIN_DATETIME).filter(spark_df.datetime < MAX_DATETIME)
    # spark_df = spark_df.limit(10000)
    # udf_findtimezone = udf(find_timezone, StringType())
    # spark_df = spark_df.withColumn("local_datetime", udf_findtimezone('datetime', 'lat', 'lon'))
    # print("tamanho spark: ", spark_df.count())
    # print("Primeiros: ", spark_df.head(5))

    datetime_column = 'datetime'
    menor = 0
    maior = 10000
    df = pd.read_csv(weeplaces_dataset)
    print("Tamanho original: ", df.shape)
    columns = ['userid', 'placeid', 'datetime', 'lat', 'lon', 'city', 'category']
    df['datetime'] = pd.to_datetime(df['datetime'], utc=True, infer_datetime_format=True)

    df = df[df[datetime_column] >= MIN_DATE]
    df = df[df[datetime_column] < MAX_DATE]

    print("Tamanho no período: ", df.shape)
    #df = df.iloc[menor:maior, :]

    # tamanho:  (7658368, 7)
    print("tamanho: ", df.shape)
    lat = df['lat'].tolist()
    lon = df['lon'].tolist()
    datetime = df['datetime'].tolist()

    tf = TimezoneFinder()
    timezones = []

    zero = dt.datetime(1980, 1, 1, 0, 0, 0)
    for i in range(len(lat)):
        if i%10000==0:
            print(i)
        timezone = tf.timezone_at(lng=lon[i], lat=lat[i])
        if timezone is None:
            timezone =  'nulo'
        elif len(timezone) == 0:
            timezone = 'nulo'
        timezones.append(timezone)

    new_datetime = []
    for i in range(len(timezones)):
        if timezones[i] == 'nulo':
            new_datetime.append(zero)
        else:
            new_datetime.append(convert_tz(datetime[i], pytz.UTC, timezones[i]))

    #print("pontos finais: ", new_datetime)
    print("pontos finais: ", len(new_datetime))
    df['local_datetime'] = np.array(new_datetime)
    df.to_csv(weeplaces_dataset_local_datetime_folder+"weeplace_checkins_local_datetime.csv", index_label=False, index=False)

    fim = time.time()

    print("Duracao: ", fim-inicio)

    # spark_df.write.csv(weeplaces_dataset_local_datetime_folder, header=True, mode='overwrite')
