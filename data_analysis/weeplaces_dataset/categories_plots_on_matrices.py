import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import copy
import datetime
import numpy as np
import json
import numpy.ma as ma
import math
from configurations import weeplaces_dataset, weeplaces_dataset_local_datetime_folder, \
    WEEPLACES_DATASET_PREPROCESSED, MONTH_7_DATE, MONTH_8_DATE, WEEPLACES_7_CATEGORIES_ADJACENCY_MATRIX, \
    WEEPLACES_7_CATEGORIES_FEATURES_MATRIX

to_osm_9_categories = {'Food': 3,
             'College & Education': 7,
             'Home / Work / Other': 1,
                'Home, Work, Others': 1,
            'Homes, Work, Others': 1,
             'Shops': 6,
             'Parks & Outdoors': 5,
             'Arts & Entertainment': 0,
             'Travel': 4,
             'Nightlife': 5,
             'Great Outdoors': 5,
             'Homes': 1,
             'Work': 2,
             'Others': 8,
             'Travel Spots': 0,
             'Colleges & Universities': 7,
             'Nightlife Spots': 5}

def select_top_k_rows(df, df_features, k):
    users_matrices = np.array([json.loads(e) for e in df['matrices'].tolist()])
    users_categories = np.array([json.loads(e) for e in df['category'].tolist()])
    users_features = np.array([json.loads(e) for e in df_features['matrices'].tolist()])
    user_id = df['user_id'].tolist()
    new_user_id = []
    new_users_matrices = []
    new_users_categories = []
    new_users_features = []
    for i in range(len(users_matrices)):
        user_matrix = np.array(users_matrices[i])
        user_category = np.array(users_categories[i])
        user_feature = np.array(users_features[i])
        if len(user_matrix) < k:
            continue
        new_user_id.append(user_id[i])
        # select the k rows that have the highest sum
        idx = top_k_rows(user_matrix, k)
        user_matrix = user_matrix[idx[:, None], idx]
        user_category = user_category[idx]
        user_feature = user_feature[idx]
        new_users_matrices.append(user_matrix)
        new_users_categories.append(user_category)
        new_users_features.append(user_feature)

    return pd.DataFrame({'user_id': new_user_id, 'matrices':new_users_matrices, 'feature': new_users_features, 'category': new_users_categories})

def top_k_rows(data, k):

    row_sum = []
    for i in range(len(data)):
        row_sum.append([np.sum(data[i]), i])

    row_sum = sorted(row_sum, reverse=True, key=lambda e:e[0])
    # if len(row_sum) > k:
    # if row_sum[k][0] < 4:
    #     print("ola")
    row_sum = row_sum[:k]

    row_sum = [e[1] for e in row_sum]

    return np.array(row_sum)

def pca(adjacencies, features, categories):

    pois_times = np.zeros((7, 7))
    pois = np.zeros((7, 7))
    adjacency_sum = np.zeros((10, 10))
    features_sum = np.zeros((10, 10))
    for i in range(len(adjacencies)):
        user_adjacency = adjacencies[i]
        user_feature = features[i]
        user_categories = categories[i]

        for j in range(user_adjacency.shape[0]):

            pass

        adjacency_sum+=user_adjacency
        features_sum+=user_feature
        #print("adj: ", user_adjacency.shape)
        #print("fea: ", user_feature.shape)
        conv_pois_times = np.matmul(user_adjacency, user_feature).T
        #print("tamanho: ", conv.shape)
        #corr = np.corrcoef(ma.masked_invalid(conv))
        corr_pois_times = pd.DataFrame(conv_pois_times).corr().to_numpy()
        #print("corr: ", corr)

        corr_pois = np.corrcoef(user_adjacency)
        for j in range(corr_pois.shape[0]):

            for k in range(j, corr_pois.shape[1]):
                if np.isnan(corr_pois[j, k]):
                    #print("entrou")
                    continue
                poi_x = user_categories[j]
                poi_y = user_categories[k]
                pois[poi_x, poi_y]+= corr_pois[j, k]
                pois[poi_y, poi_x] += corr_pois[j, k]

    maximo_pois_times = pois_times.max()
    pois_times = pois_times/maximo_pois_times
    maximo_pois = pois.max()
    pois = pois/maximo_pois
    print("Correlação pois times: ", pois_times)
    print("Correlação pois: ", pois)


if __name__ == "__main__":

    weekday = True
    hour = False
    year = False
    month = False

    portugues = True

    # df = pd.read_csv(weeplaces_dataset)
    k=10
    df = pd.read_csv(WEEPLACES_7_CATEGORIES_ADJACENCY_MATRIX)
    df_features = pd.read_csv(WEEPLACES_7_CATEGORIES_FEATURES_MATRIX)
    df = select_top_k_rows(df, df_features, k)

    count_categories = {i:0 for i in range(7)}
    categories = df['category'].tolist()
    marices = df['matrices'].tolist()
    features = df['feature'].tolist()

    for row in categories:

        for i in range(len(row)):
            count_categories[row[i]]+= 1

    print(df)
    print(count_categories)

    #pca(marices, features, categories)
    int_to_category = {0: 'Culture/Tourism',
                        1: 'Other',
                        2: 'Gastronomy',
                        3: 'Transportation',
                        4: 'Leisure',
                        5: 'Shopping',
                        6: 'Educational'}

    count_category_name = {'Culture/Tourism': 0,
                    'Other': 0,
                    'Gastronomy': 0,
                    'Transportation': 0,
                    'Leisure': 0,
                    'Shopping': 0,
                    'Educational': 0}

    for k in count_categories.keys():

        count_category_name[int_to_category[k]] = count_categories[k]

    print("cada cate", count_category_name)