import pandas as pd
import datetime

weeplaces_dataset = "/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins.csv"

WEEPLACES_DATASET_PREPROCESSED = "/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/weeplace_checkins_local_datetime_months_7_8.csv"

weeplaces_dataset_local_datetime_folder = "/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/"

MIN_DATE = pd.Timestamp(year=2010, month=6, day=29, tz='utc')
MAX_DATE = pd.Timestamp(year=2010, month=9, day=2, tz='utc')

MONTH_7_DATE = pd.Timestamp(year=2010, month=7, day=1, tz='utc')
MONTH_8_DATE = pd.Timestamp(year=2010, month=9, day=1, tz='utc')

MIN_DATETIME = datetime.datetime(2010, 6, 29, 0, 0, 0)
MAX_DATETIME = datetime.datetime(2010, 9, 2, 0, 0, 0)

WEEPLACES_ADJACENCY_MATRIX = "/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/matrizes/not_directed/adjacency_matrix_not_directed_reduced_osm.csv"

WEEPLACES_7_CATEGORIES_ADJACENCY_MATRIX = "/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/matrizes/not_directed/adjacency_matrix_not_directed_48_7_categories_osm.csv"

WEEPLACES_7_CATEGORIES_FEATURES_MATRIX = "/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/matrizes/not_directed/features_matrix_not_directed_48_7_categories_osm.csv"

COUNTRIES = "/home/claudio/Documentos/ibge_datasets/Countries_WGS84/Countries_WGS84.shp"