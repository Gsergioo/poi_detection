import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import copy
import datetime
from configurations import weeplaces_dataset, weeplaces_dataset_local_datetime_folder, \
    WEEPLACES_DATASET_PREPROCESSED, MONTH_7_DATE, MONTH_8_DATE

def count_user_weekday(e):
    dict_count = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return  dict_count

def count_user_hour(e):
    dict_count = {e:0 for e in range(24)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return  dict_count

def count_user_year(e):
    dict_count = {e:0 for e in range(2002,2012)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return  dict_count

def count_user_month(e):
    dict_count = {e:0 for e in range(1, 13)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return  dict_count

def total_events_per_user(df):

    df = df.groupby('placeid').count().reset_index()

    df.columns = ['placeid', 'count']
    df['count'] = df['count'].sort_values(ascending=False)

    if len(df) < 10:
        return pd.Series([np.nan], name='count')

    df = df.iloc[:10]

    return pd.Series([df['count'].sum()], name='count')


if __name__ == "__main__":

    weekday = False
    hour = False
    year = False
    month = False

    boxplot_total_events_per_user = True

    portugues = True

    # df = pd.read_csv(weeplaces_dataset)
    df = pd.read_csv(WEEPLACES_DATASET_PREPROCESSED)
    columns = ['userid', 'placeid', 'local_datetime', 'lat', 'lon', 'city', 'category']
    df = df[['userid', 'placeid', 'local_datetime', 'city', 'category']]
    df['local_datetime'] = pd.to_datetime(df['local_datetime'], infer_datetime_format=True, utc=True)
    print("caaaa", df['local_datetime'])

    min_date = MONTH_7_DATE
    max_date = MONTH_8_DATE

    # weekday
    if weekday:
        df_weekday = copy.copy(df)
        df_weekday = df_weekday[df_weekday['local_datetime'] >= MONTH_7_DATE]
        df_weekday = df_weekday[df_weekday['local_datetime'] < MONTH_8_DATE]
        df_weekday['local_datetime'] = df_weekday['local_datetime'].apply(lambda e: str(e.weekday()))
        ids = df_weekday['userid'].unique().tolist()
        df_weekday = df_weekday.groupby(by='userid').apply(lambda e: count_user_weekday(e['local_datetime']))
        weekdays_dicts = df_weekday.tolist()

        if portugues:
            dict_count = {0: "Segunda", 1: "Terça", 2: "Quarta", 3: "Quinta", 4: "Sexta", 5: "Sábado", 6: "Domingo"}
        else:
            dict_count = {0: "Monday", 1: "Terça", 2: "Quarta", 3: "Quinta", 4: "Friday", 5: "Saturday", 6: "Sunday"}
        value = []
        day = []
        for dict_ in weekdays_dicts:
            for e in dict_:
                value.append(dict_[e])
                day.append(dict_count[e])


        count_weekday = pd.DataFrame({'weekday': day,'value': value})
        sns.set()
        ax = plt.subplots()[1]
        if portugues:
            count_weekday['Dia da semana'] = count_weekday['weekday']
            count_weekday['Total'] = count_weekday['value']
            figure = sns.barplot(data=count_weekday, x='Dia da semana', y='Total', ax=ax, ci=95)
            figure.set_title("Eventos por dia da semana")
        else:
            count_weekday['Week day'] = df['weekday']
            count_weekday['Total'] = count_weekday['value']
            figure = sns.barplot(data=count_weekday, x='Week day', y='Total', ax=ax, ci=95)
            figure.set_title("Week per day of week")
        figure = figure.get_figure()
        if portugues:
            figure.savefig("distplot_date_meses_7_8_local_datetime.png", bbox_inches='tight', dpi=400)
        else:
            figure.savefig("distplot_date_meses_7_8_local_datetime_ingles.png", bbox_inches='tight', dpi=400)

    # hour
    if hour:
        df_hour = copy.copy(df)
        df_hour = df_hour[['userid', 'local_datetime']]
        df_hour = df_hour[df_hour['local_datetime'] >= min_date]
        df_hour = df_hour[df_hour['local_datetime'] < max_date]
        print("Describe: ", df_hour['local_datetime'].describe())
        df_hour['local_datetime'] = df_hour['local_datetime'].apply(lambda e: str(e.hour))
        df_hour = df_hour.groupby(by='userid').apply(lambda e: count_user_hour(e['local_datetime']))

        hours_dicts = df_hour.tolist()
        value = []
        hour = []
        for dict_ in hours_dicts:
            for e in dict_:
                value.append(dict_[e])
                hour.append(e)

        count_hour = pd.DataFrame({'hour': hour, 'value': value})

        ax = plt.subplots()[1]
        if portugues:
            count_hour['Hora'] = count_hour['hour']
            count_hour['Total'] = count_hour['value']
            figure = sns.barplot(data=count_hour, x='Hora', y='Total', ax=ax, ci=95)
            figure.set_title("Eventos por hora")
        else:
            count_hour['Hour'] = count_hour['hour']
            count_hour['Total'] = count_hour['value']
            figure = sns.barplot(data=count_hour, x='Hour', y='Total', ax=ax, ci=95)
            figure.set_title("Events per hour")
        figure = figure.get_figure()
        if portugues:
            figure.savefig("distplot_hora_meses_7_8_local_datetime.png", bbox_inches='tight', dpi=400)
        else:
            figure.savefig("distplot_hora_meses_7_8_local_datetime_ingles.png", bbox_inches='tight', dpi=400)

    # year
    if year:
        df_year = copy.copy(df)
        df_year['datetime'] = df['datetime'].apply(lambda e: str(e.year))
        df_year = df_year.groupby(by='userid').apply(lambda e: count_user_year(e['datetime']))

        year_dicts = df_year.tolist()
        value = []
        year = []
        for dict_ in year_dicts:
            for e in dict_:
                value.append(dict_[e])
                year.append(e)

        count_hour = pd.DataFrame({'year': year, 'value': value})
        ax = plt.subplots()[1]
        figure = sns.barplot(data=count_hour, x='year', y='value', ax=ax, ci=95)
        figure.set_title("Eventos por ano")
        figure = figure.get_figure()
        figure.savefig("distplot_year.png", bbox_inches='tight', dpi=400)

        # df_year = copy.copy(df)
        # df_year['datetime'] = df['datetime'].apply(lambda e: str(e.year))
        # df_year['datetime'] = df_year['datetime'].astype('int64')
        # ax = plt.subplots()[1]
        # figure = sns.distplot(df_year['datetime'].tolist(), ax=ax)
        # figure.set_title("Eventos por ano")
        # figure = figure.get_figure()
        # figure.savefig("distplot_year_events.png", bbox_inches='tight', dpi=400)

    # month
    if month:
        df_month = copy.copy(df)
        df_month = df_month[df_month['datetime']>datetime.datetime(year=2010, month=1, day=1)]
        df_month = df_month[df_month['datetime']<datetime.datetime(year=2011, month=1, day=1)]
        print(df_month['datetime'])
        df_month['datetime'] = df['datetime'].apply(lambda e: str(e.month))
        df_month = df_month.groupby(by='userid').apply(lambda e: count_user_month(e['datetime']))

        month_dicts = df_month.tolist()
        value = []
        month = []
        for dict_ in month_dicts:
            for e in dict_:
                value.append(dict_[e])
                month.append(e)

        count_month = pd.DataFrame({'month': month, 'value': value})
        ax = plt.subplots()[1]
        figure = sns.barplot(data=count_month, x='month', y='value', ax=ax, ci=95)
        figure.set_title("Eventos por mês (2010)")
        figure = figure.get_figure()
        figure.savefig("distplot_month.png", bbox_inches='tight', dpi=400)


    if boxplot_total_events_per_user:

        df = df[df['local_datetime'] >= MONTH_7_DATE]
        df = df[df['local_datetime'] < MONTH_8_DATE]
        df = df[['userid', 'placeid', 'category']].dropna()

        df = df.groupby('userid').apply(lambda e: total_events_per_user(e[['placeid', 'category']])).dropna().reset_index()
        print("aquii", df)
        df.columns = ['userid', 'count']

        figure = sns.boxplot(data=df['count'])
        figure = figure.get_figure()
        figure.savefig("boxplot_total_events_per_user.png", bbox_inches='tight', dpi=400)

        print("group", df['count'].describe())
