import time
import datetime as dt
import pytz
import pandas as pd
import numpy as np
from configurations import weeplaces_dataset, weeplaces_dataset_local_datetime_folder, \
    MONTH_7_DATE, MONTH_8_DATE
from timezonefinder import TimezoneFinder

# from pyspark.sql.functions import udf
# from pyspark.sql.types import StringType
# from pyspark.sql import SparkSession
# from pyspark import sql, SparkConf, SparkContext

if __name__ == "__main__":

    datetime_column = 'local_datetime'
    df = pd.read_csv(weeplaces_dataset_local_datetime_folder+"weeplace_checkins_local_datetime.csv")
    print("Tamanho original: ", df.shape)
    #columns = ['userid', 'placeid', 'datetime', 'lat', 'lon', 'city', 'category']
    df['local_datetime'] = pd.to_datetime(df['local_datetime'], utc=True, infer_datetime_format=True)

    df = df[df[datetime_column] >= MONTH_7_DATE]
    df = df[df[datetime_column] < MONTH_8_DATE]

    print("Describe: ", df['local_datetime'].describe())


    df.to_csv(weeplaces_dataset_local_datetime_folder+"weeplace_checkins_local_datetime_months_7_8.csv", index_label=False, index=False)
