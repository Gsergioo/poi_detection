import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import copy
import datetime
from configurations import weeplaces_dataset, weeplaces_dataset_local_datetime_folder, \
    WEEPLACES_DATASET_PREPROCESSED, MONTH_7_DATE, MONTH_8_DATE

to_osm_9_categories = {'Food': 3,
             'College & Education': 7,
             'Home / Work / Other': 1,
                'Home, Work, Others': 1,
            'Homes, Work, Others': 1,
             'Shops': 6,
             'Parks & Outdoors': 5,
             'Arts & Entertainment': 0,
             'Travel': 4,
             'Nightlife': 5,
             'Great Outdoors': 5,
             'Homes': 1,
             'Work': 2,
             'Others': 8,
             'Travel Spots': 0,
             'Colleges & Universities': 7,
             'Nightlife Spots': 5}

def normalize_dict(weekdays_count_list):

    for i in range(len(weekdays_count_list)):
        weekdays_count = weekdays_count_list[i]
        values = weekdays_count.values()
        total = sum(values)
        for key in weekdays_count.keys():
            weekdays_count[key] = weekdays_count[key]/total
        weekdays_count_list[i] = weekdays_count

    return pd.Series(weekdays_count_list)

def categories_to_osm(cateogries):
    #cateogries = cateogries.tolist()

    for i in range(len(cateogries)):

        for j in to_osm_9_categories.keys():
            if j in cateogries[i]:
                cateogries[i] = to_osm_9_categories[j]
                break

    return pd.Series(cateogries)


def count_user_weekday(e):
    dict_count = {"Segunda":0, "Terça":0, "Quarta":0, "Quinta":0, "Sexta":0, "Sábado":0, "Domingo":0}
    for i in range(e.shape[0]):
        dict_count[e.iloc[i]]+=1

    return  dict_count

def count_user_hour(e):
    dict_count = {e:0 for e in range(24)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return  dict_count

def count_user_year(e):
    dict_count = {e:0 for e in range(2002,2012)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return  dict_count

def count_user_month(e):
    dict_count = {e:0 for e in range(1, 13)}
    for i in range(e.shape[0]):
        dict_count[int(e.iloc[i])]+=1

    return  dict_count

if __name__ == "__main__":

    weekday = True
    hour = False
    year = False
    month = False

    portugues = True

    # df = pd.read_csv(weeplaces_dataset)
    df = pd.read_csv(WEEPLACES_DATASET_PREPROCESSED).dropna()
    columns = ['userid', 'placeid', 'local_datetime', 'lat', 'lon', 'city', 'category']
    df = df[['userid', 'placeid', 'local_datetime', 'city', 'category']]
    df['local_datetime'] = pd.to_datetime(df['local_datetime'], infer_datetime_format=True, utc=True)
    print("caaaa", df['local_datetime'])

    min_date = MONTH_7_DATE
    max_date = MONTH_8_DATE

    # weekday
    if weekday:
        int_day_to_name = {0: "Segunda", 1: "Terça", 2: "Quarta", 3: "Quinta", 4: "Sexta", 5: "Sábado", 6: "Domingo"}
        df_weekday = copy.copy(df)
        df_weekday = df_weekday[df_weekday['local_datetime'] >= MONTH_7_DATE]
        df_weekday = df_weekday[df_weekday['local_datetime'] < MONTH_8_DATE]
        df_weekday['local_datetime'] = df_weekday['local_datetime'].apply(lambda e: int_day_to_name[e.weekday()])
        df_weekday['category'] = categories_to_osm(df_weekday['category'].tolist())
        df_weekday = df_weekday.dropna()
        df_weekday = df_weekday.groupby(by='category').\
            apply(lambda e: count_user_weekday(e['local_datetime'])).reset_index()
        df_weekday.columns = ['category', 'weekdays_count']
        df_weekday['weekdays_count'] = normalize_dict(df_weekday['weekdays_count'].tolist())

        print("kaa", df_weekday)
        dict_categories = {0:'Culture/Tourism',
                     1:'Home/Hotel',
                     2:'Services',
                     3:'Gastronomy',
                     4:'Transportation',
                     5:'Leisure',
                     6:'Shopping',
                     7:'Educational',
                     8:'Other'
                     }
        value = []
        day = []

        for i in range(df_weekday.shape[0]):
            category = dict_categories[df_weekday['category'].iloc[i]]
            weekdays_count = df_weekday['weekdays_count'].iloc[i]
            print("antes", list(weekdays_count.values()))
            count_weekday = pd.DataFrame({'Dia da semana': list(weekdays_count.keys()),'Porcentagem de registros': list(weekdays_count.values())})
            sns.set()
            ax = plt.subplots()[1]
            if portugues:
                figure = sns.barplot(data=count_weekday, x='Dia da semana', y='Porcentagem de registros', ax=ax, ci=95)
                figure.set_title("Categoria "+ category+ " por dia da semana")
            else:
                count_weekday['Week day'] = df['weekday']
                count_weekday['Total'] = count_weekday['value']
                figure = sns.barplot(data=count_weekday, x='Week day', y='Total', ax=ax, ci=95)
                figure.set_title("Category per day of week")
            figure = figure.get_figure()
            if portugues:
                figure.savefig("categories_plots/distplot_category_"+ category.replace("/","_")+ "_date_meses_7_8_local_datetime.png", bbox_inches='tight', dpi=400)
            else:
                figure.savefig("distplot_category_date_meses_7_8_local_datetime_ingles.png", bbox_inches='tight', dpi=400)

    # hour
    if hour:
        df_hour = copy.copy(df)
        df_hour = df_hour[['userid', 'local_datetime']]
        df_hour = df_hour[df_hour['local_datetime'] >= min_date]
        df_hour = df_hour[df_hour['local_datetime'] < max_date]
        print("Describe: ", df_hour['local_datetime'].describe())
        df_hour['local_datetime'] = df_hour['local_datetime'].apply(lambda e: str(e.hour))
        df_hour = df_hour.groupby(by='userid').apply(lambda e: count_user_hour(e['local_datetime']))

        hours_dicts = df_hour.tolist()
        value = []
        hour = []
        for dict_ in hours_dicts:
            for e in dict_:
                value.append(dict_[e])
                hour.append(e)

        count_hour = pd.DataFrame({'hour': hour, 'value': value})

        ax = plt.subplots()[1]
        if portugues:
            count_hour['Hora'] = count_hour['hour']
            count_hour['Total'] = count_hour['value']
            figure = sns.barplot(data=count_hour, x='Hora', y='Total', ax=ax, ci=95)
            figure.set_title("Eventos por hora")
        else:
            count_hour['Hour'] = count_hour['hour']
            count_hour['Total'] = count_hour['value']
            figure = sns.barplot(data=count_hour, x='Hour', y='Total', ax=ax, ci=95)
            figure.set_title("Events per hour")
        figure = figure.get_figure()
        if portugues:
            figure.savefig("distplot_hora_meses_7_8_local_datetime.png", bbox_inches='tight', dpi=400)
        else:
            figure.savefig("distplot_hora_meses_7_8_local_datetime_ingles.png", bbox_inches='tight', dpi=400)

    # year
    if year:
        df_year = copy.copy(df)
        df_year['datetime'] = df['datetime'].apply(lambda e: str(e.year))
        df_year = df_year.groupby(by='userid').apply(lambda e: count_user_year(e['datetime']))

        year_dicts = df_year.tolist()
        value = []
        year = []
        for dict_ in year_dicts:
            for e in dict_:
                value.append(dict_[e])
                year.append(e)

        count_hour = pd.DataFrame({'year': year, 'value': value})
        ax = plt.subplots()[1]
        figure = sns.barplot(data=count_hour, x='year', y='value', ax=ax, ci=95)
        figure.set_title("Eventos por ano")
        figure = figure.get_figure()
        figure.savefig("distplot_year.png", bbox_inches='tight', dpi=400)

        # df_year = copy.copy(df)
        # df_year['datetime'] = df['datetime'].apply(lambda e: str(e.year))
        # df_year['datetime'] = df_year['datetime'].astype('int64')
        # ax = plt.subplots()[1]
        # figure = sns.distplot(df_year['datetime'].tolist(), ax=ax)
        # figure.set_title("Eventos por ano")
        # figure = figure.get_figure()
        # figure.savefig("distplot_year_events.png", bbox_inches='tight', dpi=400)

    # month
    if month:
        df_month = copy.copy(df)
        df_month = df_month[df_month['datetime']>datetime.datetime(year=2010, month=1, day=1)]
        df_month = df_month[df_month['datetime']<datetime.datetime(year=2011, month=1, day=1)]
        print(df_month['datetime'])
        df_month['datetime'] = df['datetime'].apply(lambda e: str(e.month))
        df_month = df_month.groupby(by='userid').apply(lambda e: count_user_month(e['datetime']))

        month_dicts = df_month.tolist()
        value = []
        month = []
        for dict_ in month_dicts:
            for e in dict_:
                value.append(dict_[e])
                month.append(e)

        count_month = pd.DataFrame({'month': month, 'value': value})
        ax = plt.subplots()[1]
        figure = sns.barplot(data=count_month, x='month', y='value', ax=ax, ci=95)
        figure.set_title("Eventos por mês (2010)")
        figure = figure.get_figure()
        figure.savefig("distplot_month.png", bbox_inches='tight', dpi=400)