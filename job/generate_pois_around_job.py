import pandas as pd

from foundation.configuration.input import Input
from domain.pois_around_domain import PoisAroundDomain
from domain.user_step_domain import UserStepDomain

from loader.file_loader import FileLoader


class GeneratePoisAround():

    def __init__(self):
        self.user_step_domain = UserStepDomain()
        self.pois_detected = PoisAroundDomain()
        self.file_loader = FileLoader()
    

    def start(self):
        detected_pois_filename = Input.get_arg("poi_detection_filename")
        
        detected_pois = self.user_step_domain.user_pois_from_csv(detected_pois_filename)

        """
        generate important points around pois detecteds
        """
        df_pois_around = self.pois_detected.generate_pois_around(detected_pois)
        self.file_loader.save_df_to_csv(df_pois_around, "pois_around.csv")