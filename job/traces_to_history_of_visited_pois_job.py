from foundation.abs_classes.job import Job
from domain.user_step_domain import UserStepDomain
from foundation.configuration.input import Input
from domain.traces_to_history_of_visited_pois_domain import TracesToHistoryOfVisitedPoisDomain

class TracesToHistoryOfVisitedPois:

    def __init__(self):
        self.user_step_domain = UserStepDomain()
        self.traces_of_visited_pois_domain = TracesToHistoryOfVisitedPoisDomain()


    def start(self):

        users_steps = self.user_step_domain.users_steps_from_csv()
        detected_pois = self.user_step_domain.user_pois_from_csv()
        self.traces_of_visited_pois_domain.detected_pois_validation(detected_pois, users_steps)

