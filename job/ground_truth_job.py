import pandas as pd

from foundation.configuration.input import Input
from domain.user_step_domain import UserStepDomain
from domain.ground_truth_analyze_domain import GroundTruthAnalyzeDomain
from loader.file_loader import FileLoader


class GroundTruthAnalyze():

    def __init__(self):
        self.user_step_domain = UserStepDomain()
        self.ground_truth_domain = GroundTruthAnalyzeDomain()
        self.file_loader = FileLoader()

    def start(self):
        users_step_filename = Input.get_instance().inputs["users_steps_filename"]
        classified_filename = Input.get_instance().inputs["poi_detection_filename"]
        poi_type_to_eng = Input.get_instance().inputs['poi_type_to_eng']

        users_steps = self.user_step_domain.users_steps_from_csv(users_step_filename)
        poi_classified = self.user_step_domain.user_pois_from_csv(classified_filename)
    
        #-----------------------
        """
        Create a new file with frequency and durations of ground_truth points
        """
        print("INIT")
        df_map = pd.read_csv("/home/guilherme/Documentos/Crawler_osm/csv_for_map_osm_keys.csv") ##adicionar variavel run.sh
        data = self.ground_truth_domain.extract_pois_around(poi_classified, df_map)        
        self.file_loader.save_df_to_csv(data, "/home/guilherme/Documentos/pois_keys_values_classified_points_100_meters_unique_sudeste_kkk.csv")