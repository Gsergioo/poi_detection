import pandas as pd

from domain.user_step_domain import UserStepDomain
from foundation.abs_classes.job import Job
from domain.filter_points_domain import FilterPointsDomain
from loader.file_loader import FileLoader
from foundation.configuration.input import Input

class FilterPoints:

    def __init__(self):
        self.user_step_domain = UserStepDomain()
        self.filter_points_domain = FilterPointsDomain()
        self.file_loader = FileLoader()
    
    def start(self):
        users_step_filename = Input.get_instance().inputs['users_steps_filename']
        shapefile_filename = Input.get_instance().inputs['shapefile_filename']
        
        users_steps = self.user_step_domain.users_steps_from_csv(users_step_filename)

        """
        filter coords generated in certain cities
        """
        df_with_points_filtereds = self.filter_points_domain.filter_points(users_steps, shapefile_filename)

        self.file_loader.save_df_to_csv(df_with_points_filtereds, "points_filtereds.csv")