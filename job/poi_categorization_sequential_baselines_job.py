from configuration.poi_categorization_configuration import PoICategorizationConfiguration
from configuration.poi_categorization_sequential_baselines_configuration import PoiCategorizationSequentialBaselinesConfiguration
from domain.poi_categorization_sequential_baselines_domain import PoiCategorizationSequentialBaselinesDomain
from loader.poi_categorization_sequential_baselines_loader import PoCategorizationSequentialBaselinesLoader
from foundation.configuration.input import Input

class PoiCategorizationSequentialBaselinesJob:

    def __init__(self):
        self.poi_categorization_configuration = PoICategorizationConfiguration()
        self.poi_categorization_sequential_baselines_domain = PoiCategorizationSequentialBaselinesDomain(Input.get_instance().inputs['dataset_name'])
        self.poi_categorization_sequential_baselines_loader = PoCategorizationSequentialBaselinesLoader()

    def start(self):
        dataset_name = Input.get_instance().inputs['dataset_name']
        categories_type = Input.get_instance().inputs['categories_type']
        users_sequences_filename = Input.get_instance().inputs['users_sequences']
        model_name = Input.get_instance().inputs['baseline']

        sequences_size = PoiCategorizationSequentialBaselinesConfiguration.SEQUENCES_SIZE.get_value()
        n_splits = PoiCategorizationSequentialBaselinesConfiguration.N_SPLITS.get_value()
        epochs = PoiCategorizationSequentialBaselinesConfiguration.EPOCHS.get_value()
        n_replications = PoiCategorizationSequentialBaselinesConfiguration.N_REPLICATIONS.get_value()
        batch = PoiCategorizationSequentialBaselinesConfiguration.BATCH.get_value()[model_name]

        output_base_dir = PoiCategorizationSequentialBaselinesConfiguration.OUTPUT_BASE_DIR.get_value()
        dataset_type_dir = PoiCategorizationSequentialBaselinesConfiguration.DATASET_TYPE.get_value()[dataset_name]
        category_type_dir = PoiCategorizationSequentialBaselinesConfiguration.CATEGORY_TYPE.get_value()[categories_type]
        model_name_dir = PoiCategorizationSequentialBaselinesConfiguration.MODEL_NAME.get_value()[model_name]
        location_num_classes = max(list(self.poi_categorization_configuration.DATASET_CATEGORIES_TO_INT_OSM_CATEGORIES[1][dataset_name][categories_type].values())) + 1
        class_weight = PoiCategorizationSequentialBaselinesConfiguration.CLASS_WEIGHT.get_value()[categories_type][model_name]
        optimizer = PoiCategorizationSequentialBaselinesConfiguration.OPTIMIZER.get_value()[model_name]
        output_dir = self.poi_categorization_sequential_baselines_domain.\
            output_dir(output_base_dir, dataset_type_dir, category_type_dir, model_name_dir)
        print("categorias: ", location_num_classes)
        report_model = self.poi_categorization_configuration.REPORT_MODEL[1][categories_type]


        filename=0

        users_trajectories, users_train_indexes, users_test_indexes, num_users = self.poi_categorization_sequential_baselines_domain.read_sequences(users_sequences_filename, n_splits)


        num_users +=1
        print("numero usuarios: ", num_users)
        output = output_dir + str(n_splits) + "_folds/" + str(n_replications) + "_replications/"
        folds_histories, base_report = self.poi_categorization_sequential_baselines_domain.\
            run_tests_one_location_output_k_fold(users_trajectories,
                                                users_train_indexes,
                                                users_test_indexes,
                                                n_replications,
                                                n_splits,
                                                model_name,
                                                epochs,
                                                class_weight,
                                                filename,
                                                sequences_size,
                                                report_model,
                                                 location_num_classes,
                                                 batch,
                                                 num_users,
                                                 optimizer,
                                                 output
                                                )

        print("numero classes: ", location_num_classes)
        self.poi_categorization_sequential_baselines_loader.plot_history_metrics(folds_histories, base_report, output_dir, n_splits, n_replications)
        self.poi_categorization_sequential_baselines_loader.save_report_to_csv(output_dir, base_report, n_splits, n_replications)



