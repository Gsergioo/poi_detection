import numpy as np
import pandas as pd

from domain.user_step_domain import UserStepDomain
from foundation.abs_classes.job import Job
from domain.matrix_generation_for_poi_categorization_domain import MatrixGenerationForPoiCategorizationDomain
from extractor.file_extractor import FileExtractor
from foundation.configuration.input import Input
from foundation.util.general_utils import join_df
from configuration.poi_categorization_configuration import PoICategorizationConfiguration
from configuration.matrix_generation_for_poi_categorization_configuration import MatrixGenerationForPoiCategorizationConfiguration
from loader.poi_categorization_loader import PoiCategorizationLoader

class MatrixGenerationForPoiCategorizationJob():

    def __init__(self):
        self.user_step_domain = UserStepDomain()
        self.file_extractor = FileExtractor()
        self.matrix_generation_for_poi_categorization_domain = MatrixGenerationForPoiCategorizationDomain(Input.get_instance().inputs['dataset_name'])
        self.poi_categorization_loader = PoiCategorizationLoader()
        self.poi_categorization_configuration = PoICategorizationConfiguration()

    def start(self):
        osm_category_column = None
        users_checkin_filename = Input.get_instance().inputs['users_checkin_filename']

        base_dir = Input.get_instance().inputs['base_dir']
        directed_folder = Input.get_instance().inputs['directed_folder']
        not_directed_folder = Input.get_instance().inputs['not_directed_folder']
        adjacency_matrix_base_filename = Input.get_instance().inputs['adjacency_matrix_base_filename']
        features_matrix_base_filename = Input.get_instance().inputs['features_matrix_base_filename']
        sequence_matrix_base_filename = Input.get_instance().inputs['sequence_matrix_base_filename']
        pattern_matrices = Input.get_instance().inputs['pattern_matrices']
        directed = Input.get_instance().inputs['directed']
        dataset_name = Input.get_instance().inputs['dataset_name']
        categories_type = Input.get_instance().inputs['categories_type']
        personal_matrix = Input.get_instance().inputs['personal_features_matrix']
        hour48 = Input.get_instance().inputs['hour48']
        print("Dataset: ", Input.get_instance().inputs['dataset_name'])

        if personal_matrix == "no":
            personal_matrix = False
        else:
            personal_matrix = True
        if hour48 == "no":
            hour48 = False
            hour_file = "24_"
        else:
            hour48 = True
            hour_file = "48_"

        userid_column = MatrixGenerationForPoiCategorizationConfiguration.DATASET_COLUMNS.get_value()[dataset_name]['userid_column']
        category_column = MatrixGenerationForPoiCategorizationConfiguration.DATASET_COLUMNS.get_value()[dataset_name]['category_column']
        locationid_column = MatrixGenerationForPoiCategorizationConfiguration.DATASET_COLUMNS.get_value()[dataset_name]['locationid_column']
        datetime_column = MatrixGenerationForPoiCategorizationConfiguration.DATASET_COLUMNS.get_value()[dataset_name]['datetime_column']
        latitude_column = MatrixGenerationForPoiCategorizationConfiguration.DATASET_COLUMNS.get_value()[dataset_name]['latitude_column']
        longitude_column = MatrixGenerationForPoiCategorizationConfiguration.DATASET_COLUMNS.get_value()[dataset_name]['longitude_column']

        # get list of valid categories for the given dataset
        categories_to_int_osm = self.poi_categorization_configuration.\
            DATASET_CATEGORIES_TO_INT_OSM_CATEGORIES[1][dataset_name][categories_type]
        max_size_matrices = self.poi_categorization_configuration.MAX_SIZE_MATRICES[1]
        train_size = self.poi_categorization_configuration.TRAIN_SIZE[1]
        n_splits = self.poi_categorization_configuration.N_SPLITS[1]
        n_replications = self.poi_categorization_configuration.N_REPLICATIONS[1]

        output_base_dir = self.poi_categorization_configuration.OUTPUT_DIR[1]
        dataset_type_dir = self.poi_categorization_configuration.DATASET_TYPE[1][dataset_name]
        category_type_dir = self.poi_categorization_configuration.CATEGORY_TYPE[1][categories_type]
        output_dir = output_base_dir + dataset_type_dir + category_type_dir

        base_report = self.poi_categorization_configuration.REPORT_MODEL[1][categories_type]
        users_checkin = self.file_extractor.read_csv(users_checkin_filename)
        if dataset_name == 'raw_gps':
            personal_category_column = MatrixGenerationForPoiCategorizationConfiguration.DATASET_COLUMNS.get_value()[dataset_name]['personal_category_column']
            users_checkin = users_checkin.query("" + personal_category_column + " != 'home'")
            # coluna com as categorias em um determinado raio em metros
            osm_category_column = MatrixGenerationForPoiCategorizationConfiguration.DATASET_COLUMNS.get_value()[dataset_name]['osm_category_column']

        print("coluna osm", osm_category_column)
        #----------------------

        """
        Generate matrixes for each user 
        """
        if personal_matrix:
            directed = False
            folder = base_dir + not_directed_folder
            adjacency_matrix_base_filename = folder + adjacency_matrix_base_filename + "not_directed_personal_" + hour_file + categories_type + ".csv"
            features_matrix_base_filename = folder + features_matrix_base_filename + "not_directed_personal_" + hour_file + categories_type + ".csv"
            sequence_matrix_base_filename = folder + sequence_matrix_base_filename + "not_directed_personal_" + hour_file + categories_type + ".csv"
        elif directed == "no":
            directed = False
            folder = base_dir + not_directed_folder
            adjacency_matrix_base_filename = folder+adjacency_matrix_base_filename+"not_directed_"+hour_file+categories_type+".csv"
            features_matrix_base_filename = folder+features_matrix_base_filename+"not_directed_"+hour_file+categories_type+".csv"
            sequence_matrix_base_filename = folder+sequence_matrix_base_filename+"not_directed_"+hour_file+categories_type+".csv"
        else:
            directed = True
            folder = base_dir + directed_folder
            adjacency_matrix_base_filename = folder+adjacency_matrix_base_filename + "directed_"+hour_file+categories_type+".csv"
            features_matrix_base_filename = folder+features_matrix_base_filename + "directed_"+hour_file+categories_type+".csv"
            sequence_matrix_base_filename = folder + sequence_matrix_base_filename + "directed_" + hour_file + categories_type + ".csv"

        print("arquivos: ", folder, adjacency_matrix_base_filename, features_matrix_base_filename)

        print("padrao", pattern_matrices)
        if pattern_matrices == "yes":
            self.matrix_generation_for_poi_categorization_domain\
                .generate_pattern_matrices(users_checkin,
                                           adjacency_matrix_base_filename,
                                           features_matrix_base_filename,
                                           sequence_matrix_base_filename,
                                           userid_column,
                                           category_column,
                                           locationid_column,
                                           datetime_column,
                                           categories_to_int_osm,
                                           directed,
                                           personal_matrix,
                                           hour48,
                                           osm_category_column)
        else:
            self.matrix_generation_for_poi_categorization_domain \
                .generate_gpr_matrices(users_checkin,
                                           adjacency_matrix_base_filename,
                                           features_matrix_base_filename,
                                           userid_column,
                                           latitude_column,
                                           longitude_column,
                                           category_column,
                                           locationid_column,
                                           datetime_column,
                                           categories_to_int_osm,
                                           directed,
                                           osm_category_column)
