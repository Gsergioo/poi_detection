from enum import Enum
import pytz


class MatrixGenerationForPoiCategorizationConfiguration(Enum):

    # Radius for the nearestneighbors algorithm - 100m

    CATEGORIES = ("radius", {"9": "_9categories", "12": "_12categories"}, False, "radius in meters for the nearestneighbors alogrithm")
    DATASET_COLUMNS = ("dataset_columns",
                       {'weeplaces':
                            {'userid_column':'userid',
                            'category_column':'category',
                            'locationid_column':'placeid',
                            'datetime_column':'local_datetime',
                             'latitude_column': 'lat',
                             'longitude_column': 'lon'},
                        'foursquare':
                            {'userid_column':'',
                            'category_column':'',
                            'locationid_column':'',
                            'datetime_column':''},
                        'raw_gps':
                             {'userid_column': 'userid',
                              'category_column': 'category_algorithm',
                              'personal_category_column': 'category_algorithm',
                              'locationid_column': 'poi_id',
                              'datetime_column': 'datetime',
                              'osm_category_column': 'categories_osm_100_meters'}
                         })

    # userid	placeid	datetime	lat	lon	city	category	local_datetime

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def get_key(self):
        return self.value[0]

    def get_value(self):
        return self.value[1]