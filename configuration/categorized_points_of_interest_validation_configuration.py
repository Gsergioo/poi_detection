from enum import Enum
import pytz


class CategorizedPointsOfInterestValidationConfiguration(Enum):

    # Radius for the nearestneighbors algorithm - 100m

    METERS_AROUND = ("meters", 100, False, "meters for validate categorized pois")

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def get_key(self):
        return self.value[0]

    def get_value(self):
        return self.value[1]