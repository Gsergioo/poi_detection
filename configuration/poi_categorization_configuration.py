from .base_poi_categorization_configuration import BasePoiCategorizationConfiguration

class PoICategorizationConfiguration(BasePoiCategorizationConfiguration):

    OUTPUT_DIR = ("output_dir", "output/poi_categorization_job/", False, "output directory for the poi_categorization_job")

    EPOCHS = ("epochs", 8)