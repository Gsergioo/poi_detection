from tensorflow.keras.optimizers import Adam, RMSprop, Adadelta, SGD
import tensorflow as tf

from .base_poi_categorization_configuration import BasePoiCategorizationConfiguration

class PoICategorizationBaselinesConfiguration(BasePoiCategorizationConfiguration):

    OUTPUT_BASE_DIR = ("output_dir", "output/poi_categorization_baselines_job/", False, "output directory for the poi_categorization_job")

    MODEL_NAME = ("model_name", {'gae': "gae/", 'arma': "arma/", 'arma_enhanced': "arma_enhanced/",
                                 'gcn': "gcn/", 'gat': "gat/", 'diff': "diff/",
                                 'gpr': "gpr/"})

    EPOCHS = ("epochs", {'gae': 15, 'arma': 15, 'arma_enhanced': 15, 'gcn': 15, 'gat': 15, 'diff': 15,
                         'gpr': 15})

    OPTIMIZER = ("optimizer", {'gae': Adam(), 'arma': Adam(), 'arma_enhanced': Adam(),
                               'gcn': Adam(), 'gat': Adam(), 'diff': Adam(),
                               'gpr': Adam()})

    LOSS = ("loss", {'gae': tf.keras.losses.CategoricalCrossentropy(),
                     'arma': tf.keras.losses.CategoricalCrossentropy(),
                     'arma_enhanced': tf.keras.losses.CategoricalCrossentropy(),
                     'gcn': tf.keras.losses.CategoricalCrossentropy(),
                     'gat': tf.keras.losses.CategoricalCrossentropy(),
                     'diff': tf.keras.losses.CategoricalCrossentropy(),
                     'gpr': tf.keras.losses.CategoricalCrossentropy()})

    PARAMETERS = ("parameters",
                  {'gae': {'optimizer': OPTIMIZER[1]['gae'], 'epochs': EPOCHS[1]['gae'], 'loss': LOSS[1]['gae']},
                   'arma': {'optimizer': OPTIMIZER[1]['arma'], 'epochs': EPOCHS[1]['arma'], 'loss': LOSS[1]['arma']},
                   'arma_enhanced': {'optimizer': OPTIMIZER[1]['arma'], 'epochs': EPOCHS[1]['arma'], 'loss': LOSS[1]['arma']},
                    'gcn': {'optimizer': OPTIMIZER[1]['gcn'], 'epochs': EPOCHS[1]['gcn'], 'loss': LOSS[1]['gcn']},
                    'gat': {'optimizer': OPTIMIZER[1]['gat'], 'epochs': EPOCHS[1]['gat'], 'loss': LOSS[1]['gat']},
                   'diff': {'optimizer': OPTIMIZER[1]['diff'], 'epochs': EPOCHS[1]['diff'], 'loss': LOSS[1]['diff']},
                   'gpr': {'optimizer': OPTIMIZER[1]['gpr'], 'epochs': EPOCHS[1]['gpr'], 'loss': LOSS[1]['gpr']}})

    # BASELINE_OUTPUT_DIR = ("baseline_output_dir",
    #                        {'gae':
    #                             {'osm': CATEGORY_TYPE['osm'] + "gae/",
    #                              'reduced_osm': CATEGORY_TYPE['reduced_osm'] + "gae/"},
    #                         'arma':
    #                             {'osm': CATEGORY_TYPE['osm'] + "arma/",
    #                              'reduced_osm': CATEGORY_TYPE['reduced_osm'] + "arma/"}
    #                         }, False, "output directory for baselines")