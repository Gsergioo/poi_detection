from enum import Enum
import pytz
from keras.optimizers import Nadam, Adam

class NextPlacePredictionConfiguration(Enum):

    STEP_SIZE = ("step_zize", 20, False, "size of the each step")

    LOCATION_INPUT_DIM = ("location_input_dim", 3, False, "the number of types of PoIs")
    TIME_INPUT_DIM = ("time_input_dim", 24, False, "the number of hours")

    EPOCHS = ("epochs", 8, False, "the number of epochs of training of the neural network")

    N_SPLITS = ("n_splits", 5, False, "the number of folds of k-folds cross-validation")

    CLASS_WEIGHT = ("class_weight", {0: 2, 1: 1.3, 2: 0.5}, False, "the weights of each PoI class")

    DATASET = {0: {"file_name": "location_sequence_24hours" + "usuarios.csv",
                   "time_num_classes": 24, "use_entropy": False, "entropy_num_classes": 8, "features": 3}}

    nadam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, amsgrad=False)
    OPTIMIZER = ("optimider", nadam, False, "Optimizer of the neural network")

    FIGURES_DIR = {0: "outputs/next_location_predict/figures/gru_enhaced_1location_original_10mil/"}

    VALIDATION_METRICS_NAMES = ("validation_metrics", [("loss", "val_loss", "Loss location"),
                                    ("acc", "val_acc", "Accuracy location")], False, "The metrics of the validation of "
                                                                                     "the neural network")

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def get_key(self):
        return self.value[0]

    def get_value(self):
        return self.value[1]