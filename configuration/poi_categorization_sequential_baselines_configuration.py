from enum import Enum
import pytz
from keras.optimizers import Adam, Adadelta, SGD, RMSprop, Nadam


class PoiCategorizationSequentialBaselinesConfiguration(Enum):

    # Radius for the nearestneighbors algorithm - 100m

    SEQUENCES_SIZE = ("sequences_size", 8)

    N_SPLITS = ("n_splits", 2)

    EPOCHS = ("epochs", 10)

    N_REPLICATIONS = ("n_replications", 1)

    BATCH = ("batch", {'serm': 200, 'map': 200, 'stf': 200})

    OPTIMIZER = ("learning_rate", {'serm': Adam(), 'map': Adam(), 'stf': Adam(epsilon=0.1, clipnorm=1)})

    OUTPUT_BASE_DIR = (
    "output_dir", "output/poi_categorization_sequential_baselines/", False, "output directory for the poi_categorization")

    MODEL_NAME = ("model_name", {'serm': "serm/", 'map': "map/", 'stf': "stf/"})

    DATASET_TYPE = ("dataset_type", {'foursquare': "foursquare/", 'weeplaces': "weeplaces/"})

    CATEGORY_TYPE = ("category_type",
                     {'osm': "13_categories/",
                      'reduced_osm': "9_categories/", '7_categories_osm': "7_categories/"})

    CLASS_WEIGHT = ("class_weight",
                    {'7_categories_osm': {'serm': {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1},
                                          'map': {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1},
                                          'stf': {0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1}}})


    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def get_key(self):
        return self.value[0]

    def get_value(self):
        return self.value[1]