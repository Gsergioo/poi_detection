import pandas as pd
import datetime as dt
import numpy as np
from foundation.general_code.nearest_neighbors import NearestNeighbors

from domain.pois_around_domain import PoisAroundDomain
from model.poi import Poi
from model.location_type import LocationType
from configuration.detected_points_of_interest_validation_configuration import DetectedPointsOfInterestValidationConfiguration
from configuration.categorized_points_of_interest_validation_configuration import CategorizedPointsOfInterestValidationConfiguration
from foundation.general_code.nearest_neighbors import NearestNeighbors

class GroundTruthAnalyzeDomain():
    
    def __init__(self):
        self._pois_around = PoisAroundDomain(CategorizedPointsOfInterestValidationConfiguration.METERS_AROUND.get_value())
        print(CategorizedPointsOfInterestValidationConfiguration.METERS_AROUND.get_value())

    def generate_points_durations(self, ground_truth, users_steps):
        
        ids = ground_truth["id"].unique().tolist()
        pois_durations = []
        pois_home_events = []
        pois_work_events = []
        pois_total_events = []
        mean_points_per_day = []
        median_points_per_day = []
        

        for i in ids:

            query = f"id=={i}"
            gt = ground_truth.query(query)
            us = users_steps.query(query)

            gt_latitude = gt["latitude"].tolist()
            gt_longitude = gt["longitude"].tolist()
            us_latitude = us["latitude"].tolist()
            us_longitude = us["longitude"].tolist()
            us_datetime = us["datetime"].tolist()
            us_durations = us["duration"].tolist()
            time_interval_user = (max(us_datetime) - min(us_datetime))
            days = {}

            gt_points = np.radians([(lat, long) for lat, long in zip(gt_latitude, gt_longitude)])
            us_points = np.radians([(lat, long) for lat, long in zip(us_latitude, us_longitude)])

            distances, indexes = NearestNeighbors.\
                    find_radius_neighbors(gt_points, us_points,
                                        DetectedPointsOfInterestValidationConfiguration.RADIUS.get_value())

            for j in range(len(indexes)):
                poi_coordinates = []
                poi_times = []
                poi_durations = []
                for k in range(len(indexes[j])):
                    latitude = us_latitude[indexes[j][k]]
                    longitude = us_longitude[indexes[j][k]]
                    duration = us_durations[indexes[j][k]]
                    datetime = us_datetime[indexes[j][k]]

                    poi_coordinates.append((latitude, longitude))
                    poi_times.append(datetime)
                    poi_durations.append(duration)
                if(len(poi_coordinates) == 0):
                    print(i)
                    continue
                p = Poi(poi_coordinates, poi_times, poi_durations) 
                poi_times.sort()
                index_day = poi_times[0].day + poi_times[0].month
                
                for date in poi_times:
                    if((date.day + date.month) != index_day):
                        if(index_day in days):
                            days[index_day] += 1
                        else:
                            days[index_day] = 1
                        index_day = date.day + date.month
                    
                points_per_day = sum(list(days.values()))
                mean_points_per_day.append(points_per_day/time_interval_user.days)
                

                pois_durations.append(p.poi_duration) 
                pois_home_events.append(p.n_events_home_time)
                pois_work_events.append(p.n_events_work_time)
                pois_total_events.append(p.n_events)

        ground_truth["poi_duration"] = pois_durations
        ground_truth["home_time_events"] = pois_home_events
        ground_truth["work_time_events"] = pois_work_events
        ground_truth["total_events"] = pois_total_events
        ground_truth["mean_points_per_day"] = mean_points_per_day
        
    
        return ground_truth

    def transform_pois_to_checkins(self, ground_truth, users_steps):
        total = 1
        ids = ground_truth["id"].unique().tolist()
        pois_lat = []
        pois_long = []
        pois_datetime = []
        pois_id = []
        pois_user = []
        keys = []
        values = []
        keys_values_column = []

        for i in ids[:5]:
            print(i)
            total += 1
            query = f"id=={i}"
            gt = ground_truth.query(query)
            us = users_steps.query(query)
            
            gt_latitude = gt["latitude"].tolist()
            gt_longitude = gt["longitude"].tolist()
            us_latitude = us["latitude"].tolist()
            us_longitude = us["longitude"].tolist()
            us_datetime = us["datetime"].tolist()
            us_durations = us["duration"].tolist()

            gt_points = np.radians([(lat, long) for lat, long in zip(gt_latitude, gt_longitude)])
            us_points = np.radians([(lat, long) for lat, long in zip(us_latitude, us_longitude)])

            distances, indexes = NearestNeighbors.\
                    find_radius_neighbors(gt_points, us_points,
                                        DetectedPointsOfInterestValidationConfiguration.RADIUS.get_value())
            for j in range(len(indexes)):
                poi_coordinates = []
                poi_times = []
                poi_durations = []
                for k in range(len(indexes[j])):
                    latitude = us_latitude[indexes[j][k]]
                    longitude = us_longitude[indexes[j][k]]
                    duration = us_durations[indexes[j][k]]
                    datetime = us_datetime[indexes[j][k]]

                    poi_coordinates.append((latitude, longitude))
                    poi_times.append(datetime)
                    poi_durations.append(duration)
                if(len(poi_coordinates) == 0):
                    print("locations with 0 checkins: ", i)
                    continue
                p = Poi(poi_coordinates, poi_times, poi_durations) 
                pois_user.extend([i for x in range(len(p._coordinates))])
                pois_lat.extend([ p._coordinates[x][0] for x in range (len(p._coordinates))])
                pois_long.extend([ p._coordinates[x][1] for x in range (len(p._coordinates))])
                pois_datetime.extend(p._times)
                pois_id.extend([j for x in range(len(p._coordinates))])
                categ_keys, categ_values = self._pois_around.generate_pois_around_for_unique_point(gt_latitude[j], gt_longitude[j])
                keys.extend([categ_keys for x in range(len(p._coordinates))])
                values.extend([categ_values for x in range(len(p._coordinates))])
                keys_values = self._append_keys_and_values(categ_keys, categ_values)
                keys_values_column.extend([keys_values for i in range(len(p._coordinates))])
                
        return pd.DataFrame(data={
            "user": pois_user,
            "latitude": pois_lat,
            "longitude": pois_long,
            "id": pois_id,
            "datetime": pois_datetime,
            "keys": keys,
            "values": values,
            "keys_values": keys_values_column 
        })

                       


    def _append_keys_and_values(self, keys, values):
        keys_values = []
        for i in range(len(keys)):
            key_value = keys[i] + ":" + values[i]
            keys_values.append(key_value)

        return keys_values

    def extract_pois_around(self, data: pd.DataFrame, df_map: pd.DataFrame):
        ids = data["id"].unique().tolist()
        print(ids)
        keys_values_column = []
        keys_exclusives = self._keys_exclusive_condition()
        poi_ids = []
        poi_id_column = []
        total = 0
        
        for user in ids:
            print(user)
            data_filtred = data[data["id"] == user]
            lat = data_filtred["latitude"].tolist()
            long = data_filtred["longitude"].tolist()
            coord = [(lat, long) for lat,long in zip(lat, long)]


            for i in range(len(coord)):
                keys, values = self._pois_around.generate_pois_around_for_unique_point(coord[i][0], coord[i][1])
                keys_values = self._append_keys_and_values(keys, values)
                categories = []
                
                for (key, value) in zip(keys, values):
                    if(key in keys_exclusives):
                        categories.append(keys_exclusives[key])
                    else:    
                        categorie = df_map[(df_map["categorie"] == key) & (df_map["place"] == value)]["categorie_maped"].values
                        if(len(categorie) != 0):
                            if(categorie[0] != "1"):
                                categories.append(categorie[0])
                poi_id_column.append(total)
                total += 1
                categories = list(np.unique(categories))
                keys_values_column.append(categories)

        data["categories_osm_100"] = keys_values_column
        data["poi_id"] = poi_id_column
        return data

    
    def _keys_exclusive_condition(self):
        list_with_values = {
            "tourism": "Culture/Tourism",
            "military": "Services",
            "emergency": "Services",
            "office": "Work",
            "sports": "Leisure",
            "leisure": "Leisure",
            "historic": "Culture/Tourism"
        }
        
        return list_with_values


                       




