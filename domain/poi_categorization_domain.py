import ast
import pandas as pd
import numpy as np
import json
import scipy.sparse as sp
from sklearn.preprocessing import Normalizer, MinMaxScaler
from sklearn.model_selection import KFold

import spektral as sk
from spektral.layers import ops
from spektral.utils.convolution import normalized_adjacency
from tensorflow.keras.utils import to_categorical
import tensorflow as tf
from tensorflow.keras.callbacks import EarlyStopping
import copy
import sklearn.metrics as skm
from tensorflow.keras import utils as np_utils
from functools import partial

from loader.file_loader import FileLoader
from extractor.file_extractor import FileExtractor
from model.neural_network.gnn import GNN
from utils.nn_preprocessing import user_category_to_int, one_hot_decoding, \
    one_hot_decoding_predicted, top_k_rows, weighted_categorical_crossentropy, \
    filter_data_by_valid_category



class PoiCategorizationDomain:


    def __init__(self, dataset_name):
        self.file_loader = FileLoader()
        self.file_extractor = FileExtractor()
        self.dataset_name = dataset_name

    def _selecting_categories(self, categories: pd.Series):

        unique_category = categories.unique()
        unique_category = pd.Series(unique_category)
        unique_category = unique_category.str.split(":")
        unique_category = unique_category.tolist()
        first_category = []

        for c in unique_category:
            if type(c) == list:
                first_category.append(c[0][2:])

        unique_first_category = pd.Series(first_category)
        return unique_first_category

    def _first_level_categories(self, matrices: pd.Series, categories: pd.Series, categories_to_int_osm: dict):

        if self.dataset_name == "weeplaces":
            categories = categories.tolist()
            first_categories = []
            flatten = []
            unique_first_level_categories = list(categories_to_int_osm)
            print("unicos: ", unique_first_level_categories, type(unique_first_level_categories), unique_first_level_categories[1])
            for i in range(len(categories)):
                user_categories = categories[i]
                user_first_level_categories = []
                user_categories = user_categories.split(", ")
                inicial = len(user_categories)


                user_matrix = matrices.iloc[i]
                user_matrix = json.loads(user_matrix)
                user_matrix = np.array(user_matrix)

                if inicial != user_matrix.shape[0]:
                    print("Diferentes matrix: ", inicial, user_matrix.shape[0])
                for j in range(len(user_categories)):
                    element = user_categories[j]
                    element = element.replace("[", "").replace("]", "").replace("'", "")
                    for k in range(0, len(unique_first_level_categories)):
                        first_level_category = unique_first_level_categories[k]
                        if element.find(first_level_category) != -1:
                            element = first_level_category
                            break
                        if len(element) == 1 and element == ' ':
                            element = ''
                    user_first_level_categories.append(element)
                final = len(user_first_level_categories)
                if inicial != final:
                    print("Diferentes: ", inicial, final)
                flatten = flatten + user_first_level_categories
                first_categories.append(user_first_level_categories)
            print("categorias antes: ", pd.Series(flatten).unique().tolist())
            return first_categories
        else:
            categories = categories.tolist()
            first_categories = []

            for i in range(len(categories)):
                categories[i] = categories[i].replace("[", "").replace("]", "").replace("'","").split(", ")
                first_categories.append(categories[i])
            return first_categories

    def read_matrix(self, adjacency_matrix_filename, feature_matrix_filename, sequence_matrix_filename=None):

        adjacency_df = self.file_extractor.read_csv(adjacency_matrix_filename).drop_duplicates(subset=['user_id'])
        feature_df = self.file_extractor.read_csv(feature_matrix_filename).drop_duplicates(subset=['user_id'])
        if adjacency_df['user_id'].tolist() != feature_df['user_id'].tolist():
            print("MATRIZES DIFERENTES")

        sequence_matrix_df = None
        if sequence_matrix_filename is not None:
            sequence_matrix_df = self.file_extractor.read_csv(sequence_matrix_filename).drop_duplicates(subset=['user_id'])

        return adjacency_df, feature_df, sequence_matrix_df

    def read_matrices(self, adjacency_matrix_dir, feature_matrix_dir):

        adjacency_df = self.file_extractor.read_multiples_csv(adjacency_matrix_dir)
        feature_df = self.file_extractor.read_multiples_csv(feature_matrix_dir)

        return adjacency_df, feature_df

    def read_users_metrics(self, filename):

        return self.file_extractor.read_csv(filename).drop_duplicates(subset=['user_id'])

    def _resize_adjacency_and_category_matrices(self, user_matrix, user_category, max_size_matrices, categories_type):

        k = max_size_matrices
        if user_matrix.shape[0] < k:
            k = user_matrix.shape[0]
        # select the k rows that have the highest sum
        idx = top_k_rows(user_matrix, k)
        user_matrix = user_matrix[idx[:,None], idx]
        user_category = user_category[idx]

        return user_matrix, user_category, idx

    def _resize_features_matrix(self, feature_matrix, idx, max_size_matrices):

        feature_matrix = feature_matrix[idx]
        if feature_matrix.shape[0] < max_size_matrices:
            difference = max_size_matrices - feature_matrix.shape[0]
            feature_matrix = np.pad(feature_matrix, (difference, 0), mode='constant', constant_values=0)
            feature_matrix = feature_matrix[:, :24]

        return feature_matrix

    def add_non_zero_indices(self, matrix, non_zero_indices, value):

        for i in range(len(non_zero_indices)):

            matrix[non_zero_indices[0][i]][non_zero_indices[1][i]] = matrix[non_zero_indices[0][i]][non_zero_indices[1][i]] + value

        return matrix

    def _augmentate_training_data(self, adjacency_matrices, features_matrices, categories, augmentation_cateogories: dict, user_metrics=None):

        new_adjacency_matrices = None
        new_features_matrices = None
        new_categories = None
        new_user_metrics = []
        matrix_shape = adjacency_matrices[0].shape
        print("tamaaanho", matrix_shape)


        adjacency_matrices = adjacency_matrices.tolist()
        features_matrices = features_matrices.tolist()
        categories = categories.tolist()

        for i in range(len(categories)):
            adjacency_matrix = adjacency_matrices[i]
            features_matrix = features_matrices[i]
            category = categories[i]
            if user_metrics is not None:
                user_metric = user_metrics[i]

            replication_level = 1

            if 0 in category or 6 in category:


                replication_level = 3


            # for j in range(len(augmentation_cateogories.keys())):
            #
            #     key = list(augmentation_cateogories.keys())[j]
            #     if key in category:
            #         if augmentation_cateogories[key] > replication_level:
            #             replication_level = augmentation_cateogories[key]

            # augmentation
            adjacency_non_zero_indices = np.nonzero(np.array(adjacency_matrix))
            features_non_zero_indices = np.nonzero(np.array(features_matrix))
            if replication_level > 1:
                aux = []
                aux2 = []
                aux3 = []
                for j in range(replication_level):
                    aux.append(category)
                    random_int = np.random.randint(low=2, high=10)
                    # aux2.append(self.add_non_zero_indices(matrix=adjacency_matrix,
                    #                                       non_zero_indices=adjacency_non_zero_indices, value=random_int))
                    # aux3.append(self.add_non_zero_indices(matrix=features_matrix,
                    #                                       non_zero_indices=features_non_zero_indices, value=random_int))

                    aux2.append(np.random.permutation(np.array(adjacency_matrix)).tolist())
                    aux3.append(np.random.permutation(np.array(features_matrix)).tolist())


                # for i in range(replication_level):
                #     aux.append(category)
                #     aux2.append(adjacency_matrix)
                #     aux3.append(features_matrix)
                #print("adj append", aux2)
                aux.append(category)
                category = aux
                aux2.append(adjacency_matrix)
                #print("adj append2", aux2)
                adjacency_matrix = aux2
                aux3.append(features_matrix)
                features_matrix = aux3
                #print("adj final: ", adjacency_matrix)
                if user_metrics is not None:
                    user_metric = user_metric * replication_level

            adjacency_matrix = np.array(adjacency_matrix)
            features_matrix = np.array(features_matrix)
            category = np.array(category)
            if adjacency_matrix.ndim == 2:
                adjacency_matrix = np.array([adjacency_matrix])
                features_matrix = np.array([features_matrix])

            if category.ndim == 1:
                category = np.array([category])

            if new_adjacency_matrices is None:
                new_adjacency_matrices = adjacency_matrix
                new_features_matrices = features_matrix
                new_categories = category
            else:
                new_adjacency_matrices = np.concatenate([new_adjacency_matrices, adjacency_matrix])
                new_features_matrices = np.concatenate([new_features_matrices, features_matrix])
                new_categories = np.concatenate([new_categories, category])

            if user_metrics is not None:
                new_user_metrics+= user_metric

        return new_adjacency_matrices, new_features_matrices, \
               new_categories, np.array(new_user_metrics)


    def adjacency_preprocessing(self,
                                matrix_df,
                                feature_df,
                                sequence_matrix_df,
                                users_metrics_ids,
                                osm_categories,
                                max_size_matrices,
                                max_size_sequence,
                                categories_type,
                                model_name="poi_gnn"):

        matrices_list = []
        features_matrices_list = []
        sequences_matrices_list = []

        users_categories = []
        flatten_users_categories = []
        maior = -10
        remove_users_ids = []

        ids = matrix_df['user_id'].unique().tolist()

        for i in range(matrix_df.shape[0]):
            user_id = matrix_df['user_id'].iloc[i]
            if user_id not in users_metrics_ids:
                print("diferentes", user_id)
                remove_users_ids.append(user_id)
                continue
            user_matrix = matrix_df['matrices'].iloc[i]
            user_category = matrix_df['category'].iloc[i]
            user_matrix = json.loads(user_matrix)
            user_matrix = np.array(user_matrix)
            user_category = json.loads(user_category)
            user_category = np.array(user_category)
            if user_matrix.shape[0]<max_size_matrices:
                remove_users_ids.append(user_id)
                continue
            size = user_matrix.shape[0]
            if size > maior:
                maior = size

            # matrices get new size, equal for everyone

            user_matrix, user_category, idx = self._resize_adjacency_and_category_matrices(user_matrix, user_category, max_size_matrices, categories_type)

            if model_name == "gcn" or model_name == "gae":
                user_matrix = sk.layers.GraphConv.preprocess(user_matrix)
            elif model_name == "arma" or model_name == "arma_enhanced" or model_name == "poi_gnn":
                user_matrix = sk.layers.ARMAConv.preprocess(user_matrix)
            elif model_name == "diff":
                user_matrix = sk.layers.DiffusionConv.preprocess(user_matrix)

            # feature
            user_feature_matrix = feature_df.iloc[i]
            user_feature_matrix = user_feature_matrix['matrices']
            user_feature_matrix = json.loads(user_feature_matrix)
            user_feature_matrix = np.array(user_feature_matrix)
            user_feature_matrix = user_feature_matrix[idx]
            if model_name == "poi_gnn":
                min_max_scaler = Normalizer()
                user_feature_matrix = min_max_scaler.fit_transform(user_feature_matrix.T).T

            # sequence
            if sequence_matrix_df is not None:
                user_sequence_matrix = sequence_matrix_df.iloc[i]
                user_sequence_matrix = user_sequence_matrix['matrices']
                user_sequence_matrix = json.loads(user_sequence_matrix)
                user_sequence_matrix = np.array(user_sequence_matrix)
                user_sequence_matrix = user_sequence_matrix[idx]
                if user_sequence_matrix.shape[1] < max_size_sequence:
                    remove_users_ids.append(user_id)
                    continue
                user_sequence_matrix = user_sequence_matrix[:, :max_size_sequence]
                sequences_matrices_list.append(user_sequence_matrix.tolist())

            matrices_list.append(user_matrix)
            users_categories.append(user_category)
            flatten_users_categories = flatten_users_categories + user_category.tolist()
            features_matrices_list.append(user_feature_matrix)

        self.features_num_columns = features_matrices_list[-1].shape[1]
        matrices_list = np.array(matrices_list)
        features_matrices_list = np.array(features_matrices_list)
        sequences_matrices_list = np.array(sequences_matrices_list)
        users_categories = np.array(users_categories)

        print("antes", matrices_list.shape, features_matrices_list.shape, sequences_matrices_list.shape)

        return matrices_list, users_categories, features_matrices_list, sequences_matrices_list, remove_users_ids

    def generate_nodes_ids(self, rows, cols):


        ids = []
        for i in range(rows):
            row = [i for i in range(cols)]
            ids.append(row)

        return np.array(ids)

    def k_fold_split_train_test(self,
                                adjacency_list,
                                user_categories,
                                features_list,
                                sequence_list,
                                train_size,
                                n_splits,
                                users_metrics=None):

        skip = False
        if n_splits == 1:
            skip = True
            n_splits = 2
        kf = KFold(n_splits=n_splits)

        folds = []
        classes_weights = []
        for train_indexes, test_indexes in kf.split(adjacency_list):

            fold, class_weight = self._split_train_test(adjacency_list,
                                  user_categories,
                                  features_list,
                                    sequence_list,
                                  train_size,
                                    users_metrics,
                                  train_indexes,
                                  test_indexes)
            folds.append(fold)
            classes_weights.append(class_weight)
            if skip:
                break

        return folds, classes_weights

    def _split_train_test(self,
                         adjacency_list,
                         user_categories,
                         features_list,
                          sequence_list,
                         train_size,
                          users_metrics=None,
                         train_indexes=None,
                         test_indexes=None):

        size = adjacency_list.shape[0]
        if users_metrics is not None:
            users_metrics = users_metrics[['average']].to_numpy()
        # 'average', 'cv', 'median', 'radius', 'label'
        if train_indexes is None or test_indexes is None:
            train_index = int(size*train_size)
            train_indexes = [i for i in range(0, train_index)]
            test_indexes = [i for i in range(train_index, size)]
        adjacency_list_train = adjacency_list[train_indexes]
        user_categories_train = user_categories[train_indexes]
        features_list_train = features_list[train_indexes]
        if users_metrics is not None:
            user_metrics_list_train = users_metrics[train_indexes]
        else:
            user_metrics_list_train = np.ones(shape=1)

        if len(sequence_list) > 0:
            sequence_list_train = sequence_list[train_indexes]
            sequence_list_test = sequence_list[test_indexes]
        else:
            sequence_list_train = np.array([])
            sequence_list_test = np.array([])

        adjacency_list_test = adjacency_list[test_indexes]
        user_categories_test = user_categories[test_indexes]
        features_list_test = features_list[test_indexes]
        if users_metrics is not None:
            user_metrics_list_test = users_metrics[test_indexes]
        else:
            user_metrics_list_test = np.ones(shape=1)

        flatten_train_category = user_categories_train.flatten()
        flatten_train_category = pd.Series(flatten_train_category, name='category')
        flatten_train_category = flatten_train_category.astype('object')
        train_categories_freq = {e:0 for e in flatten_train_category.unique().tolist()}
        for i in range(flatten_train_category.shape[0]):
            train_categories_freq[flatten_train_category.iloc[i]]+=1
        n = sum(train_categories_freq.values())

        total_support = 0
        for e in train_categories_freq:
            total_support+=train_categories_freq[e]

        total_support_inverse = 0
        for e in train_categories_freq:
            total_support_inverse += total_support - train_categories_freq[e]


        for e in train_categories_freq:
            train_categories_freq[e] = (total_support - train_categories_freq[e])/total_support_inverse


        class_weight = list(train_categories_freq.values())
        user_categories_train = np.array([[e for e in row] for row in user_categories_train])
        user_categories_test = np.array([[e for e in row] for row in user_categories_test])
        print("forma: ", adjacency_list_train.shape, user_categories_train.shape, user_metrics_list_train.shape,
              adjacency_list_test.shape, user_categories_test.shape, user_metrics_list_test.shape,
              sequence_list_train.shape, sequence_list_test.shape)
        return (adjacency_list_train, user_categories_train, features_list_train, sequence_list_train,
                user_metrics_list_train, adjacency_list_test, user_categories_test, features_list_test,
                sequence_list_test, user_metrics_list_test), class_weight


    def _preprocess_features(self, features):
        # print("antes: ", features[0])
        # rowsum = np.array(features.sum(1))
        # r_inv = np.power(rowsum, 1).flatten()
        # r_inv[np.isinf(r_inv)] = 0.
        # r_mat_inv = sp.diags(r_inv)
        # features = r_mat_inv.dot(features)
        # print("depois: ", features[0])
        features = normalize(features, axis=1)
        return features

    def k_fold_with_replication_train_and_evaluate_model(self,
                                                         folds,
                                                         n_replications,
                                                         classes_weights,
                                                         categories_to_int_osm,
                                                         max_size_matrices,
                                                         max_size_sequence,
                                                         base_report,
                                                         epochs):

        folds_histories = []
        folds_reports = []
        models = []
        accuracies = []
        for i in range(len(folds)):

            fold = folds[i]
            class_weight = classes_weights[i]
            histories = []
            reports = []
            adjacency_train, y_train, features_train, sequence_train, user_metrics_train, \
            adjacency_test, y_test, features_test, sequence_test, user_metrics_test = fold
            for j in range(n_replications):

                history, report, model, accuracy = self.train_and_evaluate_model(adjacency_train,
                                                        features_train,
                                                        sequence_train,
                                                        y_train,
                                                        user_metrics_train,
                                                        adjacency_test,
                                                        features_test,
                                                        sequence_test,
                                                        y_test,
                                                        user_metrics_test,
                                                        class_weight,
                                                        categories_to_int_osm,
                                                        max_size_matrices,
                                                        max_size_sequence,
                                                        epochs,
                                                        n_replications)

                base_report = self._add_location_report(base_report, report)
                histories.append(history)
                reports.append(report)
                models.append(model)
                accuracies.append(accuracy)
            folds_histories.append(histories)
            folds_reports.append(reports)
        best_model = self._find_best_model(models, accuracies)

        return folds_histories, base_report, best_model

    def train_and_evaluate_model(self,
                                 adjacency_train,
                                 features_train,
                                 sequence_train,
                                 y_train,
                                 user_metrics_train,
                                 adjacency_test,
                                 features_test,
                                 sequence_test,
                                 y_test,
                                 user_metrics_test,
                                 class_weight,
                                 categories_to_int_osm,
                                 max_size_matrices,
                                 max_size_sequence,
                                 epochs,
                                 seed,
                                 model=None):


        #print("entradas: ", adjacency_train.shape, features_train.shape, y_train.shape)
        #print("enstrada test: ", adjacency_test.shape, features_test.shape, y_test.shape)
        num_classes = len(pd.Series(list(categories_to_int_osm.values())).unique().tolist())
        max_size = max_size_matrices
        print("classes: ", num_classes, adjacency_train.shape)
        model = GNN(num_classes, max_size, max_size_sequence,
                    self.features_num_columns).build(seed=seed)
        batch = max_size*10
        print("y_train: ", y_train.shape, y_test.shape, y_train)
        print("one hot: ", np_utils.to_categorical(y_train, num_classes=num_classes).shape)
        # w1 = np.array([class_weight.values()]*max_size)
        # loss1 = partial(weighted_categorical_crossentropy, weights=w1)
        model.compile(optimizer="adam", loss=['categorical_crossentropy'],
                      weighted_metrics=[tf.keras.metrics.CategoricalAccuracy(name="acc")
                                        ])
        y_train = np_utils.to_categorical(y_train, num_classes=num_classes)
        y_test = np_utils.to_categorical(y_test, num_classes=num_classes)

        hi = model.fit(x=[adjacency_train, features_train, user_metrics_train, sequence_train],
                       y=y_train, validation_data=([adjacency_test, features_test, user_metrics_test,
                                                    sequence_test], y_test),
                       epochs=epochs, batch_size=batch,
                       shuffle=False,  # Shuffling data means shuffling the whole graph
                       callbacks=[
                           EarlyStopping(patience=50, restore_best_weights=True)
                       ]
                       )

        h = hi.history
        #print("summary: ", model.summary())

        y_predict_location = model.predict([adjacency_test, features_test, user_metrics_test, sequence_test],
                                           batch_size=batch)

        scores = model.evaluate([adjacency_test, features_test, user_metrics_test, sequence_test],
                                y_test, batch_size=batch)
        print("scores: ", scores)

        # To transform one_hot_encoding to list of integers, representing the locations
        y_predict_location = one_hot_decoding_predicted(y_predict_location)
        y_test = one_hot_decoding_predicted(y_test)
        report = skm.classification_report(y_test, y_predict_location, output_dict=True)
        # print(report)

        return h, report, model, report['accuracy']

    def _add_location_report(self, location_report, report):
        for l_key in report:
            if l_key == 'accuracy':
                location_report[l_key].append(report[l_key])
                continue
            for v_key in report[l_key]:
                location_report[l_key][v_key].append(report[l_key][v_key])

        return location_report

    def _find_best_model(self, models, accuracies):

        index = np.argmax(accuracies)
        return models[index]




        