import ast
import pandas as pd
import numpy as np
import json
import scipy.sparse as sp
from sklearn.preprocessing import normalize
from sklearn.model_selection import KFold
import spektral as sk
from spektral.layers import ops
from spektral.utils.convolution import normalized_adjacency
from tensorflow.keras.utils import to_categorical
import tensorflow as tf
from tensorflow.keras.callbacks import EarlyStopping
import copy
import sklearn.metrics as skm
from tensorflow.keras import utils as np_utils
from functools import partial

from loader.file_loader import FileLoader
from extractor.file_extractor import FileExtractor
from configuration.poi_categorization_configuration import PoICategorizationConfiguration
from model.neural_network.gnn import GNN
from utils.nn_preprocessing import user_category_to_int, one_hot_decoding, \
    one_hot_decoding_predicted, top_k_rows, weighted_categorical_crossentropy
from domain.poi_categorization_domain import PoiCategorizationDomain
from model.neural_network.poi_categorization_baselines.gae.gae import GAE
from model.neural_network.poi_categorization_baselines.gae.model import GCNModelAE
from model.neural_network.poi_categorization_baselines.arma.model import ARMAModel
from model.neural_network.poi_categorization_baselines.arma_enhanced.model import ARMAEnhancedModel
from model.neural_network.poi_categorization_baselines.gcn.model import GCN
from model.neural_network.poi_categorization_baselines.gat.model import GAT
from model.neural_network.poi_categorization_baselines.diffconv.model import DiffConv



class PoiCategorizationBaselinesDomain(PoiCategorizationDomain):


    def __init__(self, dataset_name):
        super().__init__(dataset_name)

    def find_model(self, model, num_classes, max_size, max_size_sequence, features_num_columns):
        if model == "gae":
            return GCNModelAE(num_classes, max_size, features_num_columns)
        elif model == "arma":
            return ARMAModel(num_classes, max_size, max_size_sequence, features_num_columns)
        elif model == "arma_enhanced":
            return ARMAEnhancedModel(num_classes, max_size, max_size_sequence, features_num_columns)
        elif model == "gcn":
            return GCN(num_classes, max_size, features_num_columns)
        elif model == "gat":
            return GAT(num_classes, max_size, features_num_columns)
        elif model == "diff":
            return DiffConv(num_classes, max_size, features_num_columns)

    def preprocess_adjacency_matrix_by_gnn_type(self, matrices, model_name):

        new_matrices = []
        if model_name == "gcn" or model_name == "gae":
            for i in range(len(matrices)):
                new_matrices.append(sk.layers.GraphConv.preprocess(matrices[i]))
        elif model_name == "arma" or model_name == "arma_enhanced":
            for i in range(len(matrices)):
                new_matrices.append(sk.layers.ARMAConv.preprocess(matrices[i]))
        elif model_name == "diff":
            for i in range(len(matrices)):
                new_matrices.append(sk.layers.DiffusionConv.preprocess(matrices[i]))


        return  np.array(new_matrices)

    def k_fold_with_replication_train_and_evaluate_baselines_model(self,
                                                         folds,
                                                         n_replications,
                                                         classes_weights,
                                                         categories_to_int_osm,
                                                         max_size_matrices,
                                                        max_size_sequence,
                                                         base_report,
                                                        parameters,
                                                         model_name,
                                                        augmentation_categories):

        folds_histories = []
        folds_reports = []
        iteration = 0
        for i in range(len(folds)):

            fold = folds[i]
            class_weight = classes_weights[i]
            histories = []
            reports = []
            adjacency_train, y_train, features_train, sequence_train, user_metrics_train, \
            adjacency_test, y_test, features_test, sequence_test, user_metrics_test = fold

            print("antes: ", adjacency_train.shape, y_train.shape, features_train.shape, user_metrics_train.shape)
            # adjacency_train, features_train, y_train, user_metrics_train = self._augmentate_training_data(
            #     adjacency_matrices=adjacency_train, features_matrices=features_train,
            #     categories=y_train, user_metrics=None,
            #     augmentation_cateogories=augmentation_categories)

            # adjacency_train = self.preprocess_adjacency_matrix_by_gnn_type(adjacency_train, model_name)
            # adjacency_test = self.preprocess_adjacency_matrix_by_gnn_type(adjacency_test, model_name)

            # permutation_indices = np.random.permutation(len(y_train))
            # print("reorden", permutation_indices)
            # adjacency_train = adjacency_train[permutation_indices]
            # features_train = features_train[permutation_indices]
            # y_train = y_train[permutation_indices]
            print("depois: ", adjacency_train.shape, y_train.shape, features_train.shape, user_metrics_train.shape)
            for j in range(n_replications):

                history, report = self.train_and_evaluate_baseline_model(adjacency_train,
                                                        features_train,
                                                        sequence_train,
                                                        y_train,
                                                        adjacency_test,
                                                        features_test,
                                                        sequence_test,
                                                        y_test,
                                                        categories_to_int_osm,
                                                        max_size_matrices,
                                                        max_size_sequence,
                                                        parameters,
                                                        model_name,
                                                        class_weight,
                                                        seed=iteration)
                iteration+=1

                base_report = self._add_location_report(base_report, report)
                histories.append(history)
                #reports.append(report)
            folds_histories.append(histories)
            #folds_reports.append(reports)

        return folds_histories, base_report

    def train_and_evaluate_baseline_model(self,
                                          adjacency_train,
                                          features_train,
                                          sequence_train,
                                          y_train,
                                          adjacency_test,
                                          features_test,
                                          sequence_test,
                                          y_test,
                                          categories_to_int_osm,
                                          max_size_matrices,
                                          max_size_sequence,
                                          parameters,
                                          model_name,
                                          class_weight,
                                          seed=None):


        print("entradas: ", adjacency_train.shape, features_train.shape, sequence_train.shape,
              y_train.shape)
        print("enstrada test: ", adjacency_test.shape, features_test.shape, sequence_test.shape,
              y_test.shape)
        num_classes = len(pd.Series(list(categories_to_int_osm.values())).unique().tolist())
        max_size = max_size_matrices
        print("classes: ", num_classes, adjacency_train.shape)
        batch = max_size*30
        print("tamanho batch: ", batch)
        print("epocas: ", parameters['epochs'])
        print("y_train: ", y_train.shape, y_test.shape)
        print("Modelo: ", model_name)
        model = self.find_model(model_name, num_classes, max_size, max_size_sequence, self.features_num_columns).build(output_size=num_classes, seed=seed)
        y_train = np_utils.to_categorical(y_train, num_classes=num_classes)
        y_test = np_utils.to_categorical(y_test, num_classes=num_classes)
        model.compile(optimizer=parameters['optimizer'],
                      loss=parameters['loss'],
                      weighted_metrics=[tf.keras.metrics.CategoricalAccuracy(name="acc")])


        hi = model.fit(x=[adjacency_train, features_train, sequence_train],
                       y=y_train, validation_data=([adjacency_test, features_test, sequence_test], y_test),
                       epochs=parameters['epochs'], batch_size=batch,
                       shuffle=False,  # Shuffling data means shuffling the whole graph
                       callbacks=[
                           EarlyStopping(patience=50, restore_best_weights=True)
                       ]
                       )

        h = hi.history
        #print("summary: ", model.summary())

        y_predict_location = model.predict([adjacency_test, features_test, sequence_test],
                                           batch_size=batch)

        scores = model.evaluate([adjacency_test, features_test, sequence_test],
                                y_test, batch_size=batch)
        print("scores: ", scores)

        # To transform one_hot_encoding to list of integers, representing the locations
        # print("------------- Location ------------")
        # print("saida: ", y_predict_location[0].shape, y_predict_location, y_predict_location.shape)
        y_predict_location = one_hot_decoding_predicted(y_predict_location)
        y_test = one_hot_decoding_predicted(y_test)
        # print("Original: ", y_test[0], " tamanho: ", len(y_test))
        # print("previu: ", y_predict_location[0], " tamanho: ", len(y_predict_location))
        report = skm.classification_report(y_test, y_predict_location, output_dict=True)
        # print(report)
        print("finaal", class_weight)
        return h, report




        