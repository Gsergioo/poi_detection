import geopandas as gpd
import pandas as pd

class FilterPointsDomain:

    def __init__(self):
        pass

    def filter_points(self, users_steps, shapefile_filename):

        ##dataframe with counties shapefile
        br_cities = gpd.read_file(shapefile_filename)
        query = f'NM_MUNICIP=="BELO HORIZONTE"'
        print(users_steps.head())
        counties_to_filter = br_cities.query(query) 

        geometry = gpd.points_from_xy(users_steps["longitude"], users_steps["latitude"])

        users_steps_gdf = gpd.GeoDataFrame(users_steps, geometry = geometry)
        users_steps_gdf.crs = 'epsg:4674'

        users_steps_filtered = gpd.sjoin(users_steps_gdf, counties_to_filter, how="inner", op="within")
        
        return pd.DataFrame(users_steps_filtered.drop(["geometry", "index_right"], axis = 1))