import pandas as pd
import overpy
import time
import numpy as np

from foundation.util.geospatial_utils import points_distance

class PoisAroundDomain():

    def __init__(self, meters):
        self.api = overpy.Overpass()
        self.meters_around = meters

    def generate_pois_around_all_points(self, detected_pois):
        detected_pois = detected_pois.query("poi_type=='other'")
        meters_around = 200

        df = {
            "id": [],
            "latitude": [],
            "longitude": [],
            "keys": [],
            "values": [] 
        }

        ids = detected_pois["id"].unique().tolist()
        points_around = []

        for i in ids:
            query = f"id=={i}"
            user_data = detected_pois.query(query)

            lats = user_data["latitude"].tolist()
            longs = user_data["longitude"].tolist()

            points = [(lat, long) for lat, long in zip(lats, longs)]

            for point in points:
                try:
                    api_query_nodes = f"[out:json];node(around:{meters_around},{point[0]},{point[1]})(if: count_tags() > 0)[!'highway'];(._;>;);out;"
                    api_query_ways = f"[out:json];way(around:{meters_around},{point[0]},{point[1]})(if: count_tags() > 0)[!'highway'];(._;>;);out;"

                    ways_found = self.api.query(api_query_ways)
                    time.sleep(1.5)
                    nodes_found = self.api.query(api_query_nodes)

                    for node in nodes_found.nodes:
                        tags = list(node.tags.keys())
                        values = list(node.tags.values())

                        df_keys = ", ".join(str(x) for x in tags)
                        df_values = ", ".join(str(x) for x in values)
                        
                        df["id"].append(i)
                        df["latitude"].append(point[0])
                        df["longitude"].append(point[1])
                        df["keys"].append(df_keys)
                        df["values"].append(df_values)

                    for way in ways_found.ways:
                        tags = list(way.tags.keys())
                        values = list(way.tags.values())

                        df_keys = ", ".join(str(x) for x in tags)
                        df_values = ", ".join(str(x) for x in values)
                        
                        df["id"].append(i)
                        df["latitude"].append(point[0])
                        df["longitude"].append(point[1])
                        df["keys"].append(df_keys)
                        df["values"].append(df_values)

                
                    


                except Exception as e:
                    raise e
        
        return pd.DataFrame(data=df)


            
    def generate_pois_around_for_unique_point(self, latitude, longitude):
        keys = []
        values = []
        sorted_keys = []
        sorted_values = []
        distances = []

        try:
            api_query = f"[out:json];node(around:{self.meters_around},{latitude},{longitude})(if: count_tags() > 0)[!'highway'];out;"
            api_query_ways = f"[out:json];way(around:{self.meters_around},{latitude},{longitude})(if: count_tags() > 0)[!'highway'];out;"

            nodes_found = self.api.query(api_query)
            ways_found = self.api.query(api_query_ways)

            for node in nodes_found.nodes:
                keys.append(node.tags.keys())
                values.append(node.tags.values())
                distances.append(points_distance((float(latitude), float(longitude)), (float(node.lat), float(node.lon))))
            
               
            for way in ways_found.ways:
                keys.append(way.tags.keys())
                values.append(way.tags.values())
                if(way.center_lat and way.center_lon):
                    distances.append(points_distance(latitude, longitude), (way.center_lat, way.center_lon))
                else:
                    center_lat, center_lon = self._get_way_center(way)
                    distances.append(points_distance((latitude, longitude), (center_lat, center_lon)))

            idx = np.argsort(distances)
            for i in idx:
                sorted_keys.extend(keys[i])
                sorted_values.extend(values[i])
            print(sorted_keys)
            return sorted_keys, sorted_values

        except Exception as e:
                    raise e
        
    def _get_way_center(self, way):
        _lat = 0
        _long = 0
        size = 0
        for node in way.get_nodes(resolve_missing=True):
            _lat += node.lat
            _long += node.lon
            size += 1

        latitude = _lat/size
        longitude = _long/size

        return round(float(latitude), 8), round(float(longitude), 8)
        
