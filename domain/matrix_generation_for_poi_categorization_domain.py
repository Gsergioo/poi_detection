import ast
import math
import pandas as pd
import numpy as np
import json
import scipy.sparse as sp
from sklearn.preprocessing import normalize
from sklearn.model_selection import KFold
import spektral as sk
from spektral.layers import ops
from spektral.utils.convolution import normalized_adjacency
from tensorflow.keras.utils import to_categorical
import tensorflow as tf
from tensorflow.keras.callbacks import EarlyStopping
import copy
import sklearn.metrics as skm
from tensorflow.keras import utils as np_utils
from functools import partial

from foundation.util.geospatial_utils import points_distance
from loader.matrix_generation_for_poi_categorization_loarder import MatrixGenerationForPoiCategorizationLoader
from extractor.file_extractor import FileExtractor
from model.neural_network.gnn import GNN
from utils.nn_preprocessing import user_category_to_int, one_hot_decoding, \
    one_hot_decoding_predicted, top_k_rows, weighted_categorical_crossentropy, \
    filter_data_by_valid_category



class MatrixGenerationForPoiCategorizationDomain:


    def __init__(self, dataset_name):
        self.matrix_generation_for_poi_categorization_loader = MatrixGenerationForPoiCategorizationLoader()
        self.file_extractor = FileExtractor()
        self.dataset_name = dataset_name

    def generate_pattern_matrices(self,
                                  users_checkin,
                                  adjacency_matrix_filename,
                                  features_matrix_filename,
                                  sequence_matrix_filename,
                                  userid_column,
                                  category_column,
                                  locationid_column,
                                  datetime_column,
                                  categories_to_int_osm,
                                  directed,
                                  personal_features_matrix,
                                  hour48=True,
                                  osm_category_column=None):

        if osm_category_column is not None:
            category_column = osm_category_column
        users_checkin[datetime_column] = pd.to_datetime(users_checkin[datetime_column],
                                                        infer_datetime_format=True)

        users_checkin = users_checkin.dropna(subset=[userid_column, category_column, locationid_column, datetime_column])
        ids = users_checkin[userid_column].unique().tolist()
        new_ids = []
        adj_matrices_column = []
        feat_matrices_column = []
        sequence_matrices_column = []
        categories_column = []
        count = 0
        for i in ids:
            query = userid_column+"=="+"'"+str(i)+"'"
            user_checkin = users_checkin.query(query)

            user_checkin = user_checkin.sort_values(by=[datetime_column])

            n_pois = len(user_checkin[locationid_column].unique().tolist())
            adjacency_matrix =  [[ 0 for i in range(n_pois) ] for j in range(n_pois)]
            if personal_features_matrix or not hour48:
                features_matrix = [[0 for i in range(24)] for j in range(n_pois)]
            else:
                features_matrix =  [[ 0 for i in range(48) ] for j in range(n_pois)]
            sequence_matrix = [[0 for i in range(len(user_checkin))] for j in range(n_pois)]
            categories_list = [-1 for i in range(n_pois)]

            datetimes = user_checkin[datetime_column].tolist()
            placeids = user_checkin[locationid_column].tolist()
            placeids_unique = user_checkin[locationid_column].unique().tolist()
            placeids_unique_to_int = {placeids_unique[i]: i for i in range(len(placeids_unique))}
            # converter os ids dos locais para inteiro
            placeids_int = [placeids_unique_to_int[placeids[i]] for i in range(len(placeids))]
            categories = user_checkin[category_column].tolist()
            # converter os nomes das categorias para inteiro
            if osm_category_column is not None:
                # pre-processar raw gps
                categories = self.categories_list_preproccessing(categories, categories_to_int_osm)

            else:
                categories = self.categories_preproccessing(categories, categories_to_int_osm)

            if len(categories) < 2:
                continue
            # inicializar
            if not personal_features_matrix:
                if hour48:
                    if datetimes[0].weekday() < 5:
                        hour = datetimes[0].hour
                        features_matrix[placeids_int[0]][datetimes[0].hour] += 1
                    else:
                        hour = datetimes[0].hour + 24
                        features_matrix[placeids_int[0]][datetimes[0].hour + 24] += 1
                else:
                    features_matrix[placeids_int[0]][datetimes[0].hour] += 1
            else:
                if datetimes[0].weekday() < 5:
                    features_matrix[placeids_int[0]][math.floor(datetimes[0].hour / 2)] += 1
                else:
                    features_matrix[placeids_int[0]][math.floor(datetimes[0].hour / 2) + 12] += 1
            sequence_matrix[placeids_int[0]][0] = 1
            categories_list[0] = categories[0]
            #print("tamanho: ", len(categories), len(datetimes))

            for j in range(1, len(datetimes)):
                anterior = j-1
                atual = j
                if categories[atual] == "":
                    continue
                if directed:
                    adjacency_matrix[placeids_int[anterior]][placeids_int[atual]] += 1
                else:
                    adjacency_matrix[placeids_int[anterior]][placeids_int[atual]] += 1
                    adjacency_matrix[placeids_int[atual]][placeids_int[anterior]] += 1

                if not personal_features_matrix:
                    if hour48:
                        if datetimes[atual].weekday() < 5:
                            hour = datetimes[atual].hour
                            features_matrix[placeids_int[atual]][datetimes[atual].hour] += 1
                        else:
                            hour = datetimes[atual].hour + 24
                            features_matrix[placeids_int[atual]][datetimes[atual].hour + 24] += 1
                    else:
                        features_matrix[placeids_int[atual]][datetimes[atual].hour] += 1
                else:
                    if datetimes[atual].weekday() < 5:
                        features_matrix[placeids_int[atual]][math.floor(datetimes[atual].hour/2)] += 1
                    else:
                        features_matrix[placeids_int[atual]][math.floor(datetimes[atual].hour/2) + 12] += 1

                sequence_matrix[placeids_int[atual]][j] = 1
                categories_list[placeids_int[atual]] = categories[atual]

            if osm_category_column is not None:
                # pre-processar raw gps
                adjacency_matrix, features_matrix, categories_list = self.remove_raw_gps_pois_that_dont_have_categories(categories_list, adjacency_matrix, features_matrix)
                if adjacency_matrix != []:
                    count+=1

            else:
                adjacency_matrix, features_matrix, categories_list = self.remove_gps_pois_that_dont_have_categories(
                    categories_list, adjacency_matrix, features_matrix)
                if adjacency_matrix != []:
                    count += 1

            new_ids.append(i)
            adj_matrices_column.append(str(adjacency_matrix))
            feat_matrices_column.append(str(features_matrix))
            sequence_matrices_column.append(str(sequence_matrix))
            categories_column.append(categories_list)

            #print("um usuario:\n", adjacency_matrix)

        print("Filtro: ", count)
        print("tamanhos: ", len(new_ids), len(adj_matrices_column), len(categories_column))
        adjacency_matrix_df = pd.DataFrame(data={"user_id": new_ids,
                                                "matrices": adj_matrices_column,
                                                "category": categories_column })

        features_matrix_df = pd.DataFrame(data={"user_id": new_ids,
                                                "matrices": feat_matrices_column,
                                                "category": categories_column })

        sequence_matrix_df = pd.DataFrame(data={"user_id": new_ids,
                                                "matrices": sequence_matrices_column,
                                                "category": categories_column})

        print(adjacency_matrix_df)
        print("nomee: ", features_matrix_filename, adjacency_matrix_filename)
        self.matrix_generation_for_poi_categorization_loader.\
            adjacency_features_matrices_to_csv(adjacency_matrix_df,
                                               features_matrix_df,
                                               sequence_matrix_df,
                                               adjacency_matrix_filename,
                                               features_matrix_filename,
                                               sequence_matrix_filename)

    def generate_gpr_matrices(self,
                                  users_checkin,
                                  adjacency_matrix_filename,
                                  features_matrix_filename,
                                  userid_column,
                                    latitude_column,
                                    longitude_column,
                                  category_column,
                                  locationid_column,
                                  datetime_column,
                                  categories_to_int_osm,
                                  directed,
                                  osm_category_column=None):

        print("pontos", osm_category_column)
        # constantes
        tempo_limite = 6

        if osm_category_column is not None:
            category_column = osm_category_column
        users_checkin[datetime_column] = pd.to_datetime(users_checkin[datetime_column],
                                                        infer_datetime_format=True)

        users_checkin = users_checkin.dropna(
            subset=[userid_column, category_column, locationid_column, datetime_column])
        ids = users_checkin[userid_column].unique().tolist()
        new_ids = []
        adj_matrices_column = []
        feat_matrices_column = []
        user_poi_vector_column = []
        categories_column = []
        count = 0
        it = 0
        for i in ids:
            #print(it)
            it += 1
            query = userid_column + "==" + "'" + str(i) + "'"
            user_checkin = users_checkin.query(query)

            user_checkin = user_checkin.sort_values(by=[datetime_column])

            n_pois = len(user_checkin[locationid_column].unique().tolist())
            poi_poi_graph = [[0 for i in range(n_pois)] for j in range(n_pois)]
            features_matrix = [[0 for i in range(n_pois)] for j in range(n_pois)]
            # user-poi graph (vetor que contabiliza a quantidade de visitas em cada POI)
            user_poi_vector = [0] * n_pois
            categories_list = [-1 for i in range(n_pois)]

            datetimes = user_checkin[datetime_column].tolist()
            placeids = user_checkin[locationid_column].tolist()
            latitudes = user_checkin[latitude_column].tolist()
            longitudes = user_checkin[longitude_column].tolist()
            placeids_unique = user_checkin[locationid_column].unique().tolist()
            placeids_unique_to_int = {placeids_unique[i]: i for i in range(len(placeids_unique))}
            # converter os ids dos locais para inteiro
            placeids_int = [placeids_unique_to_int[placeids[i]] for i in range(len(placeids))]
            categories = user_checkin[category_column].tolist()
            # converter os nomes das categorias para inteiro
            if osm_category_column is not None:
                # pre-processar raw gps
                categories = self.categories_list_preproccessing(categories, categories_to_int_osm)

            else:
                categories = self.categories_preproccessing(categories, categories_to_int_osm)

            if len(categories) < 2:
                continue

            # inicializar

            categories_list[0] = categories[0]
            user_poi_vector[placeids_int[0]] = 1

            for j in range(1, len(datetimes)):
                anterior = j - 1
                atual = j
                poi_anterior = placeids[anterior]
                poi_atual = placeids[atual]

                if (datetimes[atual] - datetimes[anterior]).total_seconds()/360 > tempo_limite:
                    continue

                if categories[atual] == "":
                    continue
                if directed:
                    poi_poi_graph[placeids_int[anterior]][placeids_int[atual]] += 1
                else:
                    poi_poi_graph[placeids_int[anterior]][placeids_int[atual]] += 1
                    poi_poi_graph[placeids_int[atual]][placeids_int[anterior]] += 1

                categories_list[placeids_int[atual]] = categories[atual]

                user_poi_vector[placeids_int[atual]] += 1

                # feature matrix (calcular distancia)
                if features_matrix[placeids_int[anterior]][placeids_int[atual]] != 0:
                    continue
                if features_matrix[placeids_int[anterior]][placeids_int[atual]] == 0 \
                        and placeids_int[anterior] != placeids_int[atual]:

                    di = points_distance([latitudes[anterior],
                                        longitudes[anterior]],
                                        [latitudes[atual],
                                        longitudes[atual]])

                    features_matrix[placeids_int[anterior]][placeids_int[atual]] = di
                    features_matrix[placeids_int[atual]][placeids_int[anterior]] = di

                # if osm_category_column is not None:
                #     # pre-processar raw gps
                #     poi_poi_graph, features_matrix, categories_list = self.remove_pois_that_dont_have_categories(
                #         categories_list, poi_poi_graph, features_matrix)
                #     if poi_poi_graph != []:
                #         count += 1

            poi_poi_graph, features_matrix, categories_list = self.remove_gpr_pois_that_dont_have_categories(
                categories_list, poi_poi_graph, features_matrix)
            if poi_poi_graph != []:
                count += 1

            new_ids.append(i)
            adj_matrices_column.append(str(poi_poi_graph))
            feat_matrices_column.append(str(features_matrix))
            user_poi_vector_column.append(str(user_poi_vector))
            categories_column.append(categories_list)

            # print("um usuario:\n", adjacency_matrix)

        print("Filtro: ", count)
        print("tamanhos: ", len(new_ids), len(adj_matrices_column), len(categories_column))
        adjacency_matrix_df = pd.DataFrame(data={"user_id": new_ids,
                                                 "matrices": adj_matrices_column,
                                                 "category": categories_column})

        features_matrix_df = pd.DataFrame(data={"user_id": new_ids,
                                                "matrices": feat_matrices_column,
                                                "category": categories_column})

        user_poi_matrix_df = pd.DataFrame(data={"user_id": new_ids,
                                                "vector": user_poi_vector_column})

        print(adjacency_matrix_df)
        adjacency_matrix_filename = adjacency_matrix_filename.replace("matrizes", "gpr")
        features_matrix_filename = features_matrix_filename.replace("matrizes", "gpr")

        print("nomee: ", features_matrix_filename, adjacency_matrix_filename)
        self.matrix_generation_for_poi_categorization_loader. \
            save_df_to_csv(adjacency_matrix_df, adjacency_matrix_filename)

        self.matrix_generation_for_poi_categorization_loader. \
            save_df_to_csv(features_matrix_df, features_matrix_filename)

        self.matrix_generation_for_poi_categorization_loader.\
            save_df_to_csv(user_poi_matrix_df, adjacency_matrix_filename.replace("adjacency_matrix", "user_poi_vector"))

    def _distance_between_pois(self, users_checkin, locationid_column,
                               latitude_column, longitude_column, n_pois):

        features_matrix = [[0 for i in range(n_pois)] for j in range(n_pois)]
        pois = users_checkin.groupby(locationid_column).apply(lambda e: e[[latitude_column, longitude_column]].iloc[0]).reset_index()
        print("pois: ", pois)

    def categories_preproccessing(self, categories, categories_to_int_osm):

        c = []
        for i in range(len(categories)):
            cate = categories_to_int_osm[categories[i].split(":")[0]]
            c.append(cate)

        return c

    def remove_gps_pois_that_dont_have_categories(self, categories, adjacency_matrix, features_matrix):

        indexes_filtered_pois = []
        adjacency_matrix = np.array(adjacency_matrix)
        features_matrix = np.array(features_matrix)
        for i in range(len(categories)):
            if categories[i] >= 0:
                indexes_filtered_pois.append(i)

        indexes_filtered_pois = np.array(indexes_filtered_pois)
        if len(indexes_filtered_pois) <= 1:
            return [], [], []

        categories = np.array(categories)
        categories = categories[indexes_filtered_pois]
        adjacency_matrix = adjacency_matrix[indexes_filtered_pois[:, None], indexes_filtered_pois]
        features_matrix = features_matrix[indexes_filtered_pois, :]

        if len(adjacency_matrix) <= 1:
            adjacency_matrix = []
            features_matrix = []

        return adjacency_matrix.tolist(), features_matrix.tolist(), categories.tolist()


    def remove_raw_gps_pois_that_dont_have_categories(self, categories, adjacency_matrix, features_matrix):

        indexes_filtered_pois = []
        adjacency_matrix = np.array(adjacency_matrix)
        features_matrix = np.array(features_matrix)
        for i in range(len(categories)):
            if len(categories[i]) >= 0 and categories[i][0] != "":
                indexes_filtered_pois.append(i)

        indexes_filtered_pois = np.array(indexes_filtered_pois)
        if len(indexes_filtered_pois) <= 1:
            return [], [], []

        categories = np.array(categories)
        categories = categories[indexes_filtered_pois]
        adjacency_matrix = adjacency_matrix[indexes_filtered_pois[:, None], indexes_filtered_pois]
        features_matrix = features_matrix[indexes_filtered_pois, :]

        if len(adjacency_matrix) <= 1:
            adjacency_matrix = []
            features_matrix = []

        return adjacency_matrix.tolist(), features_matrix.tolist(), categories.tolist()

    def remove_gpr_pois_that_dont_have_categories(self,
                                                  categories,
                                                  adjacency_matrix,
                                                  features_matrix):

        indexes_filtered_pois = []
        adjacency_matrix = np.array(adjacency_matrix)
        features_matrix = np.array(features_matrix)
        for i in range(len(categories)):
            if categories[i] >= 0:
                indexes_filtered_pois.append(i)

        indexes_filtered_pois = np.array(indexes_filtered_pois)
        if len(indexes_filtered_pois) <= 1:
            return [], [], []

        categories = np.array(categories)
        categories = categories[indexes_filtered_pois]
        adjacency_matrix = adjacency_matrix[indexes_filtered_pois[:, None], indexes_filtered_pois]
        features_matrix = features_matrix[indexes_filtered_pois, :]

        if len(adjacency_matrix) <= 1:
            adjacency_matrix = []
            features_matrix = []

        return adjacency_matrix.tolist(), features_matrix.tolist(), categories.tolist()

    def categories_list_preproccessing(self, categories, categories_to_int_osm):

        user_categories = []
        for i in range(len(categories)):
            c = []
            categories_names = categories[i].replace("'", "").replace(" ", "").replace("[", "").replace("]", "").split(",")
            print("element", categories_names)
            for category in categories_names:
                print("categorias: ", category)
                if category == "" or category == ' ':
                    c.append("")
                    continue
                cate = categories_to_int_osm[category]
                c.append(cate)
            user_categories.append(c)
        return user_categories