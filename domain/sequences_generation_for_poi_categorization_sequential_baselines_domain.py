import ast
import pandas as pd
import numpy as np
import networkx as nx
import json
import scipy.sparse as sp
from sklearn.preprocessing import normalize
from sklearn.model_selection import KFold
import spektral as sk
from spektral.layers import ops
from spektral.utils.convolution import normalized_adjacency
from tensorflow.keras.utils import to_categorical
import tensorflow as tf
from tensorflow.keras.callbacks import EarlyStopping
import copy
import sklearn.metrics as skm
from tensorflow.keras import utils as np_utils
from functools import partial

from loader.sequences_generation_for_poi_categorization_sequential_baselines_loader import SequencesGenerationForPoiCategorizationSequentialBaselinesLoader
from loader.matrix_generation_for_poi_categorization_loarder import MatrixGenerationForPoiCategorizationLoader
from extractor.file_extractor import FileExtractor
from model.neural_network.gnn import GNN
from utils.nn_preprocessing import user_category_to_int, one_hot_decoding, \
    one_hot_decoding_predicted, top_k_rows, weighted_categorical_crossentropy, \
    filter_data_by_valid_category



class SequencesGenerationForPoiCategorizationSequentialBaselinesDomain:

    def __init__(self, dataset_name):
        self.sequences_generation_for_poi_categorization_sequential_baselines_loader = SequencesGenerationForPoiCategorizationSequentialBaselinesLoader()
        self.file_extractor = FileExtractor()
        self.dataset_name = dataset_name

    def read_csv(self, filename, datetime_column=None):

        df = self.file_extractor.read_csv(filename)
        if datetime_column is not None:
            df[datetime_column] = pd.to_datetime(df[datetime_column], infer_datetime_format=True)
        df = df.dropna()

        return df


    def _user_checkins_to_int(self,
                              df,
                              category_column,
                              locationid_column,
                              datetime_column,
                              categories_to_int_osm,
                              max_pois):

        df = df.sort_values(by=datetime_column)

        top_k_pois = df.groupby(locationid_column). \
            apply(lambda e: pd.Series([len(e)]))
        if top_k_pois.empty:
            return pd.DataFrame({'location': ['nan'], 'hour': ['nan']})

        top_k_pois = top_k_pois.reset_index(). \
            sort_values(by=0, ascending=False)

        top_k_pois_names = top_k_pois.head(max_pois)[locationid_column].tolist()

        df = df.query(str(locationid_column) + " in " + str(top_k_pois_names))

        locations_names = df[locationid_column].unique().tolist()
        #location_to_int = {locations_names[i]:i for i in range(len(locations_names))}
        #locations = []
        hours = []
        categories = []
        for i in range(df.shape[0]):

            row = df.iloc[i]
            #location = location_to_int[row[locationid_column]]
            category = None
            for key in categories_to_int_osm.keys():
                if key in row[category_column]:
                    category = categories_to_int_osm[key]
                    break
            if category is None:
                continue
            #locations.append(location)
            categories.append(category)
            datetime = row[datetime_column]
            hour = datetime.hour
            if datetime.weekday() >= 6:
                hour = hour + 24
            hours.append(hour)

        return pd.DataFrame({'location': categories, 'hour': hours})

    def generate_sequences(self, users_checkins, sequences_size, max_pois, userid_column,
                          category_column,
                          locationid_column,
                          datetime_column,
                          categories_to_int_osm):

        #users_checkins = users_checkins.head(10000)
        #df = users_checkins.query(str(userid_column)+" == '"+str(user_id) + "'")
        df = users_checkins.groupby(userid_column).apply(lambda  e:self._user_checkins_to_int(e,
                                                                                  category_column,
                                                                                  locationid_column,
                                                                                  datetime_column,
                                                                                  categories_to_int_osm,
                                                                                    max_pois))
        print("ant", df.shape, df, df.columns)
        df = df.query("location != 'nan'")
        print("ds", df.shape, df, df.columns)
        df = self._flatten_df(df, userid_column)

        return df

    def _flatten_df(self, df, userid_column):

        indexes = df.index.values
        users_sequences = []
        users_ids = []
        for i in range(indexes.shape[0]):

            user_df = df.loc[indexes[i][0]]
            if user_df.shape[0] <=3:
                continue

            users_ids.append(i)
            user_df[userid_column] = pd.Series([i]*user_df.shape[0])

            # location/hour/userid
            users_sequences.append(user_df.to_numpy())

        df = pd.DataFrame({userid_column: users_ids, 'user_sequence': users_sequences})
        return df

    def sequences_to_csv(self, df, users_sequences_folder, dataset_name, categories_type):

        filename = users_sequences_folder+dataset_name+"_"+categories_type+"_sequences.csv"

        self.sequences_generation_for_poi_categorization_sequential_baselines_loader.sequences_to_csv(df, filename)





