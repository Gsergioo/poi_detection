import tensorflow as tf
import tensorflow.keras.backend as K
import spektral as sk
from spektral.layers.convolutional import GCNConv, ARMAConv
from spektral.layers.pooling import GlobalAttentionPool, TopKPool
from tensorflow.keras.layers import Input, Dense, Masking, Dropout, Flatten, Layer
from tensorflow.keras.models import Model
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.metrics import CategoricalAccuracy
from sklearn.decomposition import non_negative_factorization
from tensorflow.keras.regularizers import l2
import numpy as np
from spektral.layers import ops
from spektral.utils.convolution import normalized_adjacency
from tensorflow.keras import activations, initializers, regularizers, constraints

@tf.function
def user_type(type):
    tf.print("user type:", type.shape)
    if tf.less(tf.math.reduce_sum(type), int(type.shape[0]*0.3)):
        tf.print("menor", tf.math.reduce_sum(type))
        return False
    else:
        tf.print("maior")
        return True

iterations = 1          # Number of iterations to approximate each ARMA(1)
order = 2               # Order of the ARMA filter (number of parallel stacks)
share_weights = True    # Share weights in each ARMA stack
dropout = 0.5           # Dropout rate applied between layers
dropout_skip = 0.75     # Dropout rate for the internal skip connection of ARMA
l2_reg = 5e-5           # L2 regularization rate
learning_rate = 1e-2    # Learning rate
epochs = 15          # Number of training epochs
es_patience = 100       # Patience for early stopping

class AdaptativeGCN(Layer):
    def __init__(self, main_channel,
                 secondary_channel,
                 user_type_kernel_channels,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None,
                 bias_initializer='zeros',
                 bias_regularizer=None,
                 bias_constraint=None,
                 kernel_constraint=None):

        super(AdaptativeGCN, self).__init__(dynamic=True)
        self.main_channel = main_channel
        self.secondary_channel = secondary_channel
        self.user_type_kernel_channels = user_type_kernel_channels

        # self.main_layer = GCNConv(self.main_channel, activation='softmax', name='main')
        # self.main2_layer = GCNConv(self.main_channel, activation='softmax', name='main2')
        # self.secondary_layer = GCNConv(secondary_channel, activation='relu', name='secondary')

        self.main_layer = ARMAConv(self.main_channel,
                                iterations=1,
                                order=1,
                                share_weights=share_weights,
                                dropout_rate=dropout_skip,
                                activation='softmax',
                                gcn_activation=None,
                                kernel_regularizer=l2(l2_reg))
        self.main2_layer = ARMAConv(self.main_channel,
                                        iterations=1,
                                        order=1,
                                        share_weights=share_weights,
                                        dropout_rate=dropout_skip,
                                        activation='softmax',
                                        gcn_activation=None,
                                        kernel_regularizer=l2(l2_reg))
        self.secondary_layer = ARMAConv(120,
                        iterations=1,
                        order=2,
                        share_weights=True,
                        dropout_rate=0.75,
                        activation='elu',
                        gcn_activation='elu',
                        kernel_regularizer=l2(l2_reg))

        # ARMAConv(90,
        #          iterations=iterations,
        #          order=order,
        #          share_weights=share_weights,
        #          dropout_rate=dropout_skip,
        #          activation='elu',
        #          gcn_activation='elu',
        #          kernel_regularizer=l2(l2_reg))

        # self.third_layer = GCNConv(40, activation='relu', name='third')
        # self.main3_layer = GCNConv(self.main_channel, activation='softmax', name='main3')
        self.bias_regularizer = regularizers.get(bias_regularizer)
        self.bias_constraint = constraints.get(bias_constraint)

        # shared weights
        self.main_use_bias = True
        self.secondary_use_bias = True
        self.main_activation = tf.keras.activations.softmax
        self.secondary_activation = tf.keras.activations.relu
        self.bias_initializer = initializers.get(bias_initializer)


        self.kernel_initializer = initializers.get(kernel_initializer)
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.kernel_constraint = constraints.get(kernel_constraint)

    def build(self, input_shape):
        assert len(input_shape) >= 2
        input_dim = input_shape[0][-1]
        self.main_kernel = self.add_weight(shape=(input_dim, self.main_channel),
                                      initializer=self.kernel_initializer,
                                      name='main_kernel',
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)
        if self.main_use_bias:
            self.bias = self.add_weight(shape=(self.main_channel,),
                                        initializer=self.bias_initializer,
                                        name='main_bias',
                                        regularizer=self.bias_regularizer,
                                        constraint=self.bias_constraint)
        else:
            self.main_bias = None
        self.main_built = True

        self.secondary_kernel = self.add_weight(shape=(input_dim, self.secondary_channel),
                                           initializer=self.kernel_initializer,
                                           name='secondary_kernel',
                                           regularizer=self.kernel_regularizer,
                                           constraint=self.kernel_constraint)
        if self.secondary_use_bias:
            self.secondary_bias = self.add_weight(shape=(self.secondary_channel,),
                                        initializer=self.bias_initializer,
                                        name='secondary_bias',
                                        regularizer=self.bias_regularizer,
                                        constraint=self.bias_constraint)

        self.average_kernel = self.add_weight(shape=(self.main_channel,),
                                           initializer=self.kernel_initializer,
                                           name='main_kernel',
                                           regularizer=self.kernel_regularizer,
                                           constraint=self.kernel_constraint)

    def compute_output_shape(self, input_shape):
        features_shape = input_shape[0]
        output_shape = features_shape[:-1] + (self.main_channel,)
        return output_shape

    def main_convolution(self, features, fltr):

        # Convolution
        output = ops.dot(features, self.main_kernel)
        output = ops.filter_dot(fltr, output)

        if self.main_use_bias:
            output = K.bias_add(output, self.main_bias)
        if self.activation is not None:
            output = self.main_activation(output)
        return output

    def secondary_convolution(self, features, fltr):

        # Convolution
        output = ops.dot(features, self.secondary_kernel)
        output = ops.filter_dot(fltr, output)

        if self.main_use_bias:
            output = K.bias_add(output, self.secondary_bias)
        if self.activation is not None:
            output = self.secondary_activation(output)
        return output

    def call(self, inputs):

        x = inputs[0]
        a = inputs[1]
        user_metrics = inputs[2]
        x2 = inputs[3]
        #tf.print("entradas", x.shape, a.shape, user_metrics.shape, x[0], a[0], user_metrics)
        # indexes = k.argmax(user_metrics)
        # print("indexes: ", indexes)
        # indexes = tf.cast(user_metrics, dtype=tf.float64)
        # print("indexes cast: ", indexes.shape)
        average_lambda = tf.reduce_mean(user_metrics)
        average_lambda = tf.math.pow(2.71, -average_lambda)
        average = tf.keras.backend.l2_normalize(tf.keras.activations.softmax(user_metrics*self.average_kernel))
        #result = user_type(indexes)
        #result = True
        #if result:
        #self.secondary_layer.trainable = True
        #tf.print("average shape: ", average_lambda)
        secondary = self.secondary_layer([x, a])
        #tf.print("secondary shape: ", secondary)
        #secondary = tf.keras.layers.Dropout(0.5)(secondary)
        main_secondary = self.main_layer([secondary, a])
        tf.print("main_secondary shape: ", main_secondary.shape)

        main = self.main2_layer([x, a])
        tf.print("main2 shape: ", main.shape)
        #third = self.third_layer([secondary, a])

        #main3 = self.main3_layer([third, a])

        #tf.print("final: ", main.shape, main_secondary.shape)
        x = tf.math.multiply((average_lambda)*main, (1-average_lambda)*main_secondary)
        # x = K.stack([average_lambda * main, (1 - average_lambda) * main_secondary], axis=-1)
        # x = K.mean(x, axis=-1)
        # x = tf.keras.activations.softmax(x)

        return x


class GNN:

    def __init__(self, classes, max_size_matrices, max_size_sequence, features_num_columns: int):
        self.max_size_matrices = max_size_matrices
        self.max_size_sequence = max_size_sequence
        self.classes = classes
        self.features_num_columns = features_num_columns

    def build(self, seed=None):
        if seed is not None:
            tf.random.set_seed(seed)
        l2_reg = 5e-4 / 2  # L2 regularization rate
        A_input = Input((self.max_size_matrices,self.max_size_matrices))
        X_input = Input((self.max_size_matrices, self.features_num_columns))
        Metrics_input = Input((1))
        S_input = Input((self.max_size_matrices, self.max_size_sequence))
        kernel_channels = 2

        x2 = ARMAConv(120,
                 iterations=iterations,
                 order=order,
                 share_weights=share_weights,
                 dropout_rate=dropout_skip,
                 activation='elu',
                 gcn_activation='elu',
                 kernel_regularizer=l2(l2_reg))([S_input, A_input])
        x2 = Dropout(0.5)(x2)

        x2 = tf.keras.layers.Concatenate()([X_input, x2])

        out = AdaptativeGCN(main_channel=self.classes, secondary_channel=120,
                            user_type_kernel_channels=kernel_channels)([x2, A_input, Metrics_input, x2])
        # out = GCNConv(self.classes, activation='softmax')([out, A_input])
        #out = Dense(self.classes, activation='softmax')(x)

        model = Model(inputs=[A_input, X_input, Metrics_input, S_input], outputs=[out])

        return model

