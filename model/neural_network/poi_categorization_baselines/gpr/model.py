import tensorflow as tf
from tensorflow.python.framework import tensor_shape
from tensorflow.keras.layers import Concatenate, Dense, Dropout, Layer, LayerNormalization
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Dropout
from tensorflow.keras.regularizers import l2
from spektral.layers.convolutional import ARMAConv
from tensorflow.keras import activations, initializers, regularizers, constraints

iterations = 1          # Number of iterations to approximate each ARMA(1)
order = 2               # Order of the ARMA filter (number of parallel stacks)
share_weights = True    # Share weights in each ARMA stack
dropout = 0.5           # Dropout rate applied between layers
dropout_skip = 0.75     # Dropout rate for the internal skip connection of ARMA
l2_reg = 5e-5           # L2 regularization rate
learning_rate = 1e-2    # Learning rate
epochs = 15          # Number of training epochs
es_patience = 100       # Patience for early stopping

# @tf.function
# def divide(input):
#
#     e = input[0]
#     n = input[1]
#
#     tf.print("entradas2", e, n)
#     p = []
#
#     for i in range(e.shape[1]):
#         tensor = []
#         for j in range(e[i].shape[0]):
#             aux = e[i][j]/n[i][j]
#             tensor.append(aux)
#
#         p.append(tensor)
#
#     return p

class GGLR_unit(Layer):
    def __init__(self, main_channel):

        super(GGLR_unit, self).__init__()
        self.main_channel = main_channel

        self.main_kernel = Dense(main_channel)
        self.normalization = LayerNormalization()

    def call(self, inputs, **kwargs):

        x = inputs[0]
        #x_sum = tf.reduce_sum(x, axis=1)
        x_sum = tf.keras.backend.sum(x, axis=1)
        x_sum = x_sum + 0.0000000000001
        x_out = self.main_kernel(x)

        x_out = self.normalization(x_out)
        #x_out = tf.keras.layers.Lambda(divide)([x_out, x_sum])

        #tf.print("dentro", x_out)
        x_out = tf.keras.layers.LeakyReLU()(x_out)
        #tf.print("dentro2", x_out)

        return x_out

class GPR_unit(Layer):
    def __init__(self, main_channel,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None,
                 bias_initializer='zeros',
                 bias_regularizer=None,
                 bias_constraint=None,
                 kernel_constraint=None):

        super(GPR_unit, self).__init__()
        self.main_channel = main_channel

        self.dense1 = Dense(main_channel, use_bias=False)
        self.dense2 = Dense(main_channel)

    def call(self, inputs, **kwargs):

        p_out = inputs[0]
        user = inputs[1]

        #user = tf.matmul(user, self.main_kernel) # 10x10
        user_out = self.dense1(user)

        # p_out = tf.matmul(p_out, self.secondary_kernel) # 10x
        # p_out = tf.add(p_out, self.main_bias)
        p_out_out = self.dense2(p_out)
        user_out = tf.add(user_out, p_out_out)

        return tf.keras.activations.relu(user_out)

class GPR_component(Layer):
    def __init__(self, main_channel):

        super(GPR_component, self).__init__()
        self.main_channel = main_channel

        self.gglr_unit_in = GGLR_unit(main_channel)
        self.gglr_unit_out = GGLR_unit(main_channel)
        self.glr_unit = GPR_unit(main_channel)

    def call(self, inputs, **kwargs):

        p_in = inputs[0]
        p_out = inputs[1]
        user = inputs[2]

        p_in_out = self.gglr_unit_in([p_in])
        p_out_out = self.gglr_unit_out([p_out])
        gpr_out = self.glr_unit([p_out_out, user])

        #tf.print("componestes: ", p_in_out, p_out_out)

        return (p_in_out, p_out_out, gpr_out)
        #return [tf.constant(1, shape=(10,10)), tf.constant(1, shape=(10,10)), tf.constant(1, shape=(10,10))]

class exibir(Layer):
    def __init__(self, units=1):
        super(exibir, self).__init__()


    def call(self, inputs):

        x = inputs[0]
        x1 = inputs[1]
        tf.print("camada: ", x)

        return x1

class GPRModel:
    def __init__(self, classes, max_size_matrices, features_num_columns: int):
        self.max_size_matrices = max_size_matrices
        self.classes = classes
        self.features_num_columns = features_num_columns

    def build(self, units1=10, output_size=7, dropout=0.5, seed=None):
        if seed is not None:
            tf.random.set_seed(seed)
        A_input = Input((self.max_size_matrices, self.max_size_matrices))
        A_transposed_input = Input((self.max_size_matrices, self.max_size_matrices))
        X_input = Input((self.max_size_matrices, self.features_num_columns))
        U_input = Input((self.max_size_matrices))

        p_in_out1, p_out_out1, gpr_out1 = GPR_component(units1)([A_transposed_input, A_input, U_input])

        p_in_out2, p_out_out2, gpr_out2 = GPR_component(units1)([p_in_out1, p_out_out1, gpr_out1])

        out = Concatenate()([p_in_out2, p_out_out2])
        out = Dense(output_size, use_bias=False, activation='softmax')(out)

        model = Model(inputs=[A_transposed_input, A_input, X_input, U_input], outputs=[out])

        return model


