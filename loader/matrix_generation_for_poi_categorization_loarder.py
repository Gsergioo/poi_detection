from pathlib import Path
from matplotlib import pyplot
import numpy as np
import pandas as pd

from loader.file_loader import FileLoader

class MatrixGenerationForPoiCategorizationLoader(FileLoader):

    def __init__(self):
        pass

    def adjacency_features_matrices_to_csv(self, adjacency,
                                           features,
                                           sequece,
                                           adjacency_filename,
                                           features_filename,
                                           sequence_filename):

        self.save_df_to_csv(adjacency, adjacency_filename)
        self.save_df_to_csv(features, features_filename)
        self.save_df_to_csv(sequece, sequence_filename)
