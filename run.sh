# !/bin/bash
echo "Argument: "$1

# common variables
BASE_DIR="/home/claudio/Documentos/users_steps_datasets/tipo_0_2/user_steps_tipo_0_2/"
USERS_STEPS_FILENAME="/home/guilherme/Documentos/Users_filtrados_por_regiao/frequent_users.csv"
POI_DETECTION_FILENAME="/home/guilherme/Documentos/users_detected_pois.csv"
POI_CLASSIFICATION_FILENAME="/home/guilherme/Documentos/users_classified_pois.csv"
GROUND_TRUTH="/home/guilherme/Documentos/Users_filtrados_por_regiao/Users_rotulated/rotulated_manual_edit.csv"
SHAPEFILE_FILENAME="${BASE_DIR}/BR_shapefile/BRMUE250GC_SIR.shp"
USERS_CHECKIN_FILENAME="${BASE_DIR}/weeplaces/weeplace_1000.csv"
USERS_CHECKIN_FILENAME_BASELINE_HMRM="/home/guilherme/Documentos/Analisar_dataset_weeplaces_7_8_meses/users_7_8_months_clean.csv"
FEATURES_FILENAME_HMRM="/home/guilherme/Documentos/new_users_steps_dataset/features_hmrm_500_users.csv"

# Weeplaces configuration
WEEPLACES_BASE_DIR="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/"
WEEPLACES_DATASET="${WEEPLACES_BASE_DIR}weeplace_checkins_local_datetime_months_7_8.csv"
WEEPLACES_DIRECTED_FOLDER="matrizes/directed/"
WEEPLACES_NOT_DIRECTED_FOLDER="matrizes/not_directed/"
ADJACENCY_MATRIX_BASE_FILENAME="adjacency_matrix_"
FEATURES_MATRIX_BASE_FILENAME="features_matrix_"
SEQUENCE_MATRIX_BASE_FILENAME="sequence_matrix_"
WEEPLACES_NOT_DIRECTED_ADJACENCY_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/matrizes/not_directed/adjacency_matrix_not_directed_48_7_categories_osm.csv"
WEEPLACES_NOT_DIRECTED_FEATURE_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/matrizes/not_directed/features_matrix_not_directed_48_7_categories_osm.csv"
WEEPLACES_SEQUENCES_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/matrizes/not_directed/sequence_matrix_not_directed_48_7_categories_osm.csv"
WEEPLACES_DIRECTED_ADJACENCY_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/matrizes/directed/adjacency_matrix_directed_7_categories_osm.csv"
WEEPLACES_DIRECTED_FEATURE_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/matrizes/directed/features_matrix_directed_7_categories_osm.csv"
WEEPLACES_USER_METRICS_FILENAME="/home/claudio/Documentos/pycharmprojects/projeto_guilherme/poi_detection/files/weeplaces_user_metrics.csv"


# raw gps
RAW_GPS_BASE_DIR="/media/claudio/Acer/Users/claud/Downloads/doutorado/raw_gps/"
RAW_GPS_DATASET="${RAW_GPS_BASE_DIR}raw_gps_pois_checkins.csv"
RAW_GPS_ADJACENCY_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/raw_gps/not_directed/adjacency_matrix_directed_7_categories_osm.csv"
RAW_GPS_FEATURE_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/raw_gps/not_directed/features_matrix_directed_7_categories_osm.csv"
RAW_GPS_GROUND_TRUTH="${RAW_GPS_BASE_DIR}categories_maped_all_distances.csv"
RAW_GPS_DIRECTED_FOLDER="${RAW_GPS_BASE_DIR}directed/"
RAW_GPS_NOT_DIRECTED_FOLDER="${RAW_GPS_BASE_DIR}not_directed/"
RAW_GPS_USER_METRICS_FILENAME="raw_gps_user_metrics.csv"

# sequences configuration
USERS_SEQUENCES_FOLDER="/media/claudio/Acer/Users/claud/Downloads/doutorado/sequences_poi_categorization/"
WEEPLACES_USERS_SEQUENCES_FILENAME="${USERS_SEQUENCES_FOLDER}weeplaces_7_categories_osm_sequences.csv"

# weeplaces gpr
WEEPLACES_GPR_DIRECTED_ADJACENCY_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/gpr/directed/adjacency_matrix_directed_48_7_categories_osm.csv"
WEEPLACES_GPR_DIRECTED_FEATURE_MATRIX_FILENAME="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/gpr/directed/features_matrix_directed_48_7_categories_osm.csv"
WEEPLACES_GPR_USER_POI_VECTOR="/media/claudio/Acer/Users/claud/Downloads/doutorado/weeplaces/weeplace_checkins_local_datetime/gpr/directed/user_poi_vector_directed_48_7_categories_osm.csv"

# Foursquare configuration
FOURSQUARE_ADJACENCY_MATRIX_FOLDER="/home/claudio/Documentos/datasets_redes_neurais/fourquare_2012_2013/nye/"
FOURSQUARE_FEATURE_MATRIX_FOLDER="/home/claudio/Documentos/datasets_redes_neurais/fourquare_2012_2013/nye/"
FOURSQUARE_FEATURE_MATRIX_FILENAME="${FOURSQUARE_FEATURE_MATRIX_FOLDER}features_matrix.csv"
FOURSQUARE_ADJACENCY_MATRIX_FILENAME="${FOURSQUARE_ADJACENCY_MATRIX_FOLDER}adjacency_matrix.csv"
FOURSQUARE_NYE_USER_METRICS_FILENAME="/home/claudio/Documentos/pycharmprojects/projeto_guilherme/poi_detection/files/foursquare_nye_user_metrics.csv"

# points_of_interest_job
# This job is applied to detect users' pois
POI_CONFIG='{
          "job": "points_of_interest_job",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "poi_classification_filename": "'$POI_CLASSIFICATION_FILENAME'",
          "ground_truth": "'$GROUND_TRUTH'",
          "utc_to_sp": "no", 
          "poi_type_to_eng": "no"
          }'

# points_of_interest_validation_job
# This job is applied to validate the pois found by the points_of_interest_job
VALIDATION_CONFIG='{
          "job": "points_of_interest_validation_job",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "poi_classification_filename": "'$POI_CLASSIFICATION_FILENAME'",
          "ground_truth": "'$GROUND_TRUTH'",
          "poi_type_to_eng": "no"
          }'
GROUND_TRUTH_CONFIG='{
          "job": "analyze_ground_truth",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "poi_classification_filename": "'$POI_CLASSIFICATION_FILENAME'",
          "ground_truth": "'$GROUND_TRUTH'",
          "poi_type_to_eng": "no"
          }'
POIS_AROUND_CONFIG='{
          "job": "generate_pois_around",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "ground_truth": "'$GROUND_TRUTH'"
          }'
FILTER_POINTS_CONFIG='{
          "job": "filter_points",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "ground_truth": "'$GROUND_TRUTH'",
          "shapefile_filename": "'$SHAPEFILE_FILENAME'"
          }'

SEQUENCE_GENERATION_FOR_POI_CATEGORIZATION_SEQUENTIAL_CONFIG='{
          "job": "sequences_generation_for_poi_categorization_sequential_baselines_job",
          "users_checkin_filename": "'$WEEPLACES_DATASET'",
          "base_dir":"'$WEEPLACES_BASE_DIR'",
          "users_sequences_folder":"'$USERS_SEQUENCES_FOLDER'",
          "dataset_name":"weeplaces",
          "categories_type":"7_categories_osm"
          }'

POI_CATEGOIZATION_SEQUENTIAL_BASELINES='{
          "job": "poi_categorization_sequential_baselines",
          "users_sequences": "'$WEEPLACES_USERS_SEQUENCES_FILENAME'",
          "baseline": "serm",
          "dataset_name":"weeplaces",
          "categories_type":"7_categories_osm"
          }'

# categories_type = 'osm' or 'reduced_osm'
#MATRIX_GENERATION_FOR_POI_CATEGORIZATION_CONFIG='{
#          "job": "matrix_generation_for_poi_categorization",
#          "users_checkin_filename": "'$WEEPLACES_DATASET'",
#          "base_dir":"'$WEEPLACES_BASE_DIR'",
#          "directed_folder":"'$WEEPLACES_DIRECTED_FOLDER'",
#          "not_directed_folder":"'$WEEPLACES_NOT_DIRECTED_FOLDER'",
#          "adjacency_matrix_base_filename":"'$ADJACENCY_MATRIX_BASE_FILENAME'",
#          "features_matrix_base_filename":"'$FEATURES_MATRIX_BASE_FILENAME'",
#          "directed":"no",
#          "dataset_name":"weeplaces",
#          "categories_type":"7_categories_osm"
#          }'

MATRIX_GENERATION_FOR_POI_CATEGORIZATION_CONFIG='{
          "job": "matrix_generation_for_poi_categorization",
          "users_checkin_filename": "'$WEEPLACES_DATASET'",
          "base_dir":"'$WEEPLACES_BASE_DIR'",
          "directed_folder":"'$WEEPLACES_DIRECTED_FOLDER'",
          "not_directed_folder":"'$WEEPLACES_NOT_DIRECTED_FOLDER'",
          "adjacency_matrix_base_filename":"'$ADJACENCY_MATRIX_BASE_FILENAME'",
          "features_matrix_base_filename":"'$FEATURES_MATRIX_BASE_FILENAME'",
          "hour48":"yes",
          "personal_features_matrix":"no",
          "sequence_matrix_base_filename":"'$SEQUENCE_MATRIX_BASE_FILENAME'",
          "directed":"yes",
          "dataset_name":"weeplaces",
          "pattern_matrices":"no",
          "categories_type":"7_categories_osm"
          }'

CATEGORIZATION_CONFIG='{
          "job": "categorization",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "ground_truth": "'$GROUND_TRUTH'",
          "users_checkin_filename": "'$WEEPLACES_DATASET'",
          "adjacency_matrix_filename": "'$WEEPLACES_NOT_DIRECTED_ADJACENCY_MATRIX_FILENAME'",
          "feature_matrix_filename": "'$WEEPLACES_NOT_DIRECTED_FEATURE_MATRIX_FILENAME'",
          "sequence_matrix":"'$WEEPLACES_SEQUENCES_MATRIX_FILENAME'",
          "user_metrics":"'$WEEPLACES_USER_METRICS_FILENAME'",
          "graph_type":"not_directed",
          "dataset_name":"weeplaces",
          "categories_type":"7_categories_osm"
          }'
BASELINES_CONFIG='{
          "job": "hmrm_baseline",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "users_steps_filename": "'$USERS_STEPS_FILENAME_BASELINE_HMRM'",
          "ground_truth": "'$GROUND_TRUTH'",
          "users_checkin_filename": "'$USERS_CHECKIN_FILENAME_BASELINE_HMRM'",
          "features_filename": "'$FEATURES_FILENAME_HMRM'",
          "weeplaces?": "Yes"
          }'

CATEGORIZATION_BASELINES_CONFIG='{
          "job": "categorization_baselines",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "ground_truth": "'$GROUND_TRUTH'",
          "adjacency_matrix_filename": "'$WEEPLACES_NOT_DIRECTED_ADJACENCY_MATRIX_FILENAME'",
          "feature_matrix_filename": "'$WEEPLACES_NOT_DIRECTED_FEATURE_MATRIX_FILENAME'",
          "sequence_matrix":"'$WEEPLACES_SEQUENCES_MATRIX_FILENAME'",
          "user_metrics":"'$WEEPLACES_USER_METRICS_FILENAME'",
          "graph_type":"not_directed",
          "dataset_name":"weeplaces",
          "categories_type":"7_categories_osm",
          "baseline":"gae"
          }'
POI_CATEGORIZATION_VALIDATION='{
          "job": "poi_categorization_validation",
          "poi_categorized_filename": "'$POI_DETECTION_FILENAME'",
          "ground_truth": "'$GROUND_TRUTH'",
  }'

CATEGORIZATION_BASELINE_GPR_CONFIG='{
          "job": "poi_categorization_baseline_gpr",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "adjacency_matrix_filename": "'$WEEPLACES_GPR_DIRECTED_ADJACENCY_MATRIX_FILENAME'",
          "feature_matrix_filename": "'$WEEPLACES_GPR_DIRECTED_FEATURE_MATRIX_FILENAME'",
          "user_poi_vector_filename": "'$WEEPLACES_GPR_USER_POI_VECTOR'",
          "user_metrics":"'$WEEPLACES_USER_METRICS_FILENAME'",
          "graph_type":"directed",
          "dataset_name":"weeplaces",
          "categories_type":"7_categories_osm"
          }'

TRANSFER_LEARNING_CONFIG='{
          "job": "categorization",
          "poi_detection_filename": "'$POI_DETECTION_FILENAME'",
          "users_steps_filename": "'$USERS_STEPS_FILENAME'",
          "ground_truth": "'$RAW_GPS_GROUND_TRUTH'",
          "users_checkin_filename": "'$WEEPLACES_DATASET'",
          "adjacency_matrix_filename": "'$RAW_GPS_ADJACENCY_MATRIX_FILENAME'",
          "feature_matrix_filename": "'$RAW_GPS_FEATURE_MATRIX_FILENAME'",
          "user_metrics":"'$RAW_GPS_USER_METRICS_FILENAME'",
          "graph_type":"not_directed",
          "dataset_name":"weeplaces",
          "categories_type":"7_categories_osm"
          }'

CATEGORIZATION_PERFORMANCE_GRAPHICS_CONFIG='{
          "job": "categorization_performance_graphics",
          "dataset_name":"weeplaces",
          "categories_type":"7_categories_osm",
          "graph_type":"not_directed",
          "folds":"5",
          "replications":"2"
          }'


echo $CONFIG

case $1 in
  "find_poi")
    python main.py "${POI_CONFIG}"
    ;;
  "validate")
    python main.py "${VALIDATION_CONFIG}"
    ;;
  "find_poi_and_validate")
    python main.py "${POI_CONFIG}"
    python main.py "${VALIDATION_CONFIG}"
    ;;
  "analyze_ground_truth")
    python main.py "${GROUND_TRUTH_CONFIG}"
    ;;
  "generate_pois_around")
    python main.py "${POIS_AROUND_CONFIG}"
    ;;
  "filter_points")
    python main.py "${FILTER_POINTS_CONFIG}"
    ;;
  "categorization")
    python main.py "${CATEGORIZATION_CONFIG}"
    ;;
  "categorization_baselines")
    python main.py "${CATEGORIZATION_BASELINES_CONFIG}"
    ;;
  "categorization_performance")
    python main.py "${CATEGORIZATION_PERFORMANCE_GRAPHICS_CONFIG}"
    ;;
  "matrix_generation")
    python main.py "${MATRIX_GENERATION_FOR_POI_CATEGORIZATION_CONFIG}"
    ;;
  "sequence_generation")
    python main.py "${SEQUENCE_GENERATION_FOR_POI_CATEGORIZATION_SEQUENTIAL_CONFIG}"
    ;;
  "hmrm_baseline")
    python main.py "${BASELINES_CONFIG}"
    ;;
  "poi_categorization validation")
    python main.py "${POI_CATEGORIZATION_VALIDATION}"
    ;;
  "sequential_baselines")
    python main.py "${POI_CATEGOIZATION_SEQUENTIAL_BASELINES}"
    ;;
  "categorization_gpr")
    python main.py "${CATEGORIZATION_BASELINE_GPR_CONFIG}"
    ;;
  "transfer")
    python main.py "${TRANSFER_LEARNING_CONFIG}"
    ;;
esac